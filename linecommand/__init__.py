from django.contrib.auth.models import User
from linecommand.models import Clients, Numbers, Accounts

User.USERNAME_FIELD = 'email'
User._meta.get_field_by_name('email')[0]._unique = True
User._meta.get_field_by_name('username')[0]._unique = False
User.REQUIRED_FIELDS.remove('email')
