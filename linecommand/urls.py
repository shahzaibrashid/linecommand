from django.conf.urls import patterns, include, url
from linecommand import views
from data import *

urlpatterns = patterns('',
                       # Examples:
                       # Index
                       url(r'^$', views.Index, name='index'),
                       # Paypal
                       url(r'^(?P<client_id>\d+)/paypal/$', views.paypal, name='paypal'),
                       url(r'^(?P<client_id>\d+)/paypal/success/$', views.paypal_success, name='paypal_success'),
                       # Client Lists
                       url(r'^list/', views.ClientList, name='client_list'),
                       # Creat Client
                       url(r'^create/$', views.Create, name='createclient'),
                       # Import numbers
                       url(r'^import/$', views.NumbersImport, name='import_numbers'),
                       # Deposit
                       url(r'^(?P<client_id>\d+)/deposit/$', views.Deposit, name='deposit'),
                       # Modify Client
                       url(r'^(?P<client_id>\d+)/modify/$', views.SetPrice, name='modify'),
                       # Buy Numbers
                       url(r'^(?P<client_id>\d+)/numbers/$', views.PurchaseNumbers, name='numbers'),
                       url(r'^(?P<client_id>\d+)/displaynum/$', views.ShowNumbers, name='shownumbers'),
                       #url(r'^(?P<client_id>\d+)/details/$', views.Details, name='detail'),
                       # Client dashboard
                       url(r'^(?P<client_id>\d+)/details/$', views.Client_main, name='client_main'),
                       # Client buy numbers
                       url(r'^(?P<client_id>\d+)/details/buyNumbers/$', views.Client_buyNumbers, name='client_numbers'),
                       url(r'^(?P<client_id>\d+)/details/createClient/$', views.Client_createClient,
                           name='client_create'),
                       url(r'^(?P<client_id>\d+)/details/exportNumbers/$', views.Client_exportNumbers,
                           name='export_numbers'),
                       url(r'^(?P<client_id>\d+)/details/referalLink/$', views.Client_Referal,
                           name='client_referal'),
                       url(r'^(?P<client_id>\d+)/details/randNumbers/$', views.Client_randomNumbers,
                           name='client_randnumbers'),
                       url(r'^(?P<client_id>\d+)/details/geoNumbers/$', views.Client_GeoNumbers,
                           name='client_geonumbers'),
                       url(r'^(?P<client_id>\d+)/details/tempNumbers/$', views.Client_NonclNumbers,
                           name='client_buynoncl'),
                       # Client SMS Management
                       url(r'^(?P<client_id>\d+)/details/smsManagement/$', views.Client_sms_management,
                           name='client_sms'),
                       # Maintenance
                       # url(r'^(?P<client_id>\d+)/details/geoNumbers/$', views.Maintenance, name='client_geonumbers'),
                       # acc history
                       # Client History
                       url(r'^(?P<client_id>\d+)/details/accHistory/$', views.Client_History, name='client_history'),
                       # Client Fundings
                       url(r'^(?P<client_id>\d+)/details/fundings/$', views.Client_Funding, name='client_funding'),
                       # Support
                       url(r'^(?P<client_id>\d+)/details/support/$', views.VideoTutorial, name='video_tutorial'),
                       # Call Reporting
                       url(r'^(?P<client_id>\d+)/details/call_reporting/$', views.client_cdr, name='cdr'),
                       url(r'^(?P<client_id>\d+)/details/call_reporting/(?P<num>\d+)$',
                           views.client_cdr_detail, name='cdr_detail'
                       ),
                       # Settings
                       url(r'^(?P<client_id>\d+)/details/settings/$', views.Client_Settings, name='client_settings'),
                       #url(r'^(?P<client_id>\d+)/details/editHtml/$', views.Client_editCode, name='client_editCode'),
                       # Area Codes
                       url(r'^(?P<state>\w+)/areacodes/$', views.AreaCodes, name='load_areacodes'),
                       # Login / Logout
                       url(r'^login/', 'django.contrib.auth.views.login', {'template_name': 'linecommand/login.html'}),
                       url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/login/'}, name='logout'),
                       # Sign Up
                       url(r'^signup/$', views.SignUp, name='signup'),
                       url(r'^signup/(?P<uidb36>[0-9A-Za-z]+)/$', views.ReferalSignUp, name='referal_signup'),
                       # Password reset
                       url(r'^password/reset/$', 'django.contrib.auth.views.password_reset',
                           {'post_reset_redirect': 'password_reset_done',
                            'template_name': 'linecommand/password_reset_form.html',
                            'email_template_name': 'linecommand/password_reset_email.html'},
                           name='password_reset'),
                       url(r'^password/reset/done/$', 'django.contrib.auth.views.password_reset_done',
                           {'template_name': 'linecommand/password_reset_done.html'},
                           name='password_reset_done'),
                       url(r'^password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                           'django.contrib.auth.views.password_reset_confirm',
                           {'post_reset_redirect': 'password_reset_complete',
                            'template_name': 'linecommand/password_reset_confirm.html'},
                           name='password_reset_confirm'),
                       url(r'^password/done/$', 'django.contrib.auth.views.password_reset_complete',
                           {'template_name': 'linecommand/password_reset_complete.html'},
                           name='password_reset_complete')
)

urlpatterns += patterns('',
    url(r'^something/hard/to/guess/', include('paypal.standard.ipn.urls')),
    url(r'^985e12aa-db0f-4b83-88fd-8edd1717404e/$', views.showstats, name='export_stats'),
)

urlpatterns += patterns('',
    url(r'^api/v1.0/geo/Orders/$', Orders.as_view(), name='Orders')
)