__author__ = 'shahzaib'

from django.core.management.base import BaseCommand, CommandError
from linecommand.bandwidth_v2 import INetwork

class Command(BaseCommand):
    args = '<area_code,area_code,... area_code,area_code,... ...>'
    help = 'Checks status of order and updates it.'

    def handle(self, *args, **kwargs):
        inetwork = INetwork()
        for area_codes in args:
            self.stdout.write('Checking area codes "%s"' % area_codes)
            quantity = 500
            numbers = inetwork.Search_Numbers(quantity, area_codes)
            self.stdout.write("Requested %d numbers, Got %d numbers" % (quantity, len(numbers)))