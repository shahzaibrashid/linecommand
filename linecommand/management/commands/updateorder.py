__author__ = 'shahzaib'

from django.core.management.base import BaseCommand, CommandError
from linecommand.models import Numbers, GeoOrders, Accounts, Clients
from linecommand.bandwidth_v2 import INetwork
from datetime import datetime
from os.path import basename


class Command(BaseCommand):
    help = 'Checks status of order and updates it.'

    def handle(self, *args, **options):
        orders = GeoOrders.objects.filter(status=1)
        inetwork = INetwork()
        for order in orders:
            self.stdout.write('Checking Order "%s"' % basename(order.order_url))
            numbers = Numbers.objects.filter(geo_status=order.id)
            if numbers:
                client = numbers[0].client
                completed_numbers = []
                status, pending = inetwork.Check_Order(order.order_url, completed_numbers)
                if status:
                    num_count = len(completed_numbers)
                    if num_count > 0:
                        order.status = 0
                        order.date_resolved = datetime.now()
                        order.save()
                        update_number = numbers.filter(
                            number__in=completed_numbers
                        ).update(
                            geo_status=0
                        )
                        delete_numbers = numbers.filter(geo_status=order.id)
                        if delete_numbers:
                            deleted_numbers_count = {30:0, 90:0}
                            for num in delete_numbers:
                                duration = num.duration
                                if duration == 30:
                                    deleted_numbers_count[30] += 1
                                else:
                                    deleted_numbers_count[90] += 1
                                delete_status = num.delete()
                            ancestors = client.get_ancestors(True, True)
                            for obj in ancestors:
                                if obj.user.is_superuser:
                                    continue
                                refund_amount = (
                                    obj.geo_landline_30 * deleted_numbers_count[30]
                                ) + (
                                    obj.geo_landline_90 * deleted_numbers_count[90]
                                )
                                acc_update = Accounts.objects.create(
                                    client=client,
                                    amount=refund_amount,
                                    transaction_type='Refunded due to order failure'
                                )
                                acc_update.save()
                        self.stdout.write('Order Processed')
                    else:
                        self.stdout.write('Order is still pending')