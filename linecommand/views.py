# Create your views here.
from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.shortcuts import Http404, get_object_or_404, render, redirect, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.utils.http import base36_to_int, int_to_base36
from django.db.models import Count, Sum
from django.db import connection
from django.db.models import Q
from django.template.loader import render_to_string
from django.core.mail import send_mail
from linecommand.forms import CreateClient, DepositForm, ChangePrice, AddNumbers, BalanceForm, UpdateNumbersForm
from linecommand.forms import ChangeUserForm, ExportNumbers, RandomNumbersForm, RenewNumbersForm, ImportNumbers, AddGeo
from linecommand.forms import SignUpForm, ReferalSignUpForm
from linecommand.forms import HtmlCode, paypal_api, fund_account, UpdateSMSTypeForm
from linecommand.models import Clients, Accounts, Numbers, Areas, UserHtml, GeoOrders, ForwardingLogs
from datetime import datetime, timedelta, date
import bandwidth
import bandwidth_v2
import random
import string
import os
import re
import hashlib
import csv
import logging

logger = logging.getLogger(__name__)


def validate_number(input_number, forward=True):
    out_number = None
    err = True
    if forward:
        out_number = re.sub(r'[\s\'\"\(\)-]', '', input_number)
    else:
        match = re.search(r'[%s]' % string.letters, input_number)
        if match:
            out_number = re.sub(r'[\s\'\"\(\)]', '', input_number)
        else:
            out_number = re.sub(r'[\s\'\"\(\)-]', '', input_number)
    if not out_number:
        err = False
    elif forward:
        if out_number.startswith('+1'):
            out_number = out_number.lstrip('+1')
        elif out_number.startswith('001'):
            out_number = out_number.lstrip('001')
        elif out_number.startswith('1') and len(out_number) > 10:
            out_number = out_number.lstrip('1')
        if out_number.startswith('911'):
            err = False
    else:
        err = True
    return err, out_number


def get_total_price(client, numbers_count, duration):
    amount = 0.0
    if duration == 30:
        for elem in numbers_count:
            if elem.get('provider') == 'TTC' or elem.get('provider') == 'MICHIGAN':
                amount += (client.landline_30 * client.discount_factor) * elem.get('num_count', 0)
            else:
                amount += (client.geo_landline_30 * client.discount_factor) * elem.get('num_count', 0)
    elif duration == 60:
        for elem in numbers_count:
            if elem.get('provider') == 'TTC' or elem.get('provider') == 'MICHIGAN':
                amount += (client.landline_60 * client.discount_factor) * elem.get('num_count', 0)
            else:
                amount += (client.geo_landline_90 * client.discount_factor) * elem.get('num_count', 0)
    else:
        for elem in numbers_count:
            if elem.get('provider') == 'TTC' or elem.get('provider') == 'MICHIGAN':
                amount += (client.landline_90 * client.discount_factor) * elem.get('num_count', 0)
            else:
                amount += (client.geo_landline_90 * client.discount_factor) * elem.get('num_count', 0)
    return amount


def dictfetchall(cursor):
    """Returns all rows from a cursor as a dict"""
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


def SignUp(request):
    form = SignUpForm(request.POST or None)
    if form.is_valid():
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        username = hashlib.md5(email).hexdigest()[2:]
        name = form.cleaned_data['name']
        address = form.cleaned_data['billing_address']
        skype_id = form.cleaned_data['skype_id']
        aol_id = form.cleaned_data['aol_id']
        phone = form.cleaned_data['phone']
        user_new = User.objects.create_user(
            username=username,
            email=email,
            password=password
        )
        # user_new.save()
        client = Clients.objects.create(
            name=name,
            address=address,
            skype=skype_id,
            aol_id=aol_id,
            phone=phone,
            binded=0,
            user=user_new,
            parent=Clients.objects.get(pk=1)
        )
        client.save()
        user = authenticate(username=email, password=password)
        if user is not None:
            login(request, user)
        return redirect(reverse('client_main', args=(client.id,)))

    args = {}
    args.update(csrf(request))
    args['signUpForm'] = form
    return render_to_response('linecommand/signup.html', args,
                              context_instance=RequestContext(request))


def ReferalSignUp(request, uidb36=None,
                  template_name='linecommand/signup.html'):
    assert uidb36 is not None  # checked by URLconf
    try:
        uid_int = base36_to_int(uidb36)
        client = Clients.objects.get(pk=uid_int)
    except (ValueError, OverflowError, Clients.DoesNotExist):
        client = Clients.objects.get(pk=1)
    if client is not None:
        if request.POST:
            form = ReferalSignUpForm(request.POST)
            if form.is_valid():
                new_client = form.save(client)
                user = authenticate(
                    username=form.cleaned_data['email'],
                    password=form.cleaned_data['password']
                )
                if user is not None:
                    login(request, user)
                return redirect(reverse('client_main', args=(new_client.id,)))
        else:
            form = ReferalSignUpForm(None)
    args = {}
    args.update(csrf(request))
    args['signUpForm'] = form
    args['referal'] = True
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))

@login_required(login_url="/login/")
def Index(request):
    node = get_object_or_404(Clients, user=request.user)
    if node.is_leaf_node():
        return redirect(reverse('client_main', args=(node.id,)))
    #client_display = node.get_children().order_by('-id')
    args = {}
    if 'update_username' in request.POST:
        user_change_form = ChangeUserForm(request.POST)
        if user_change_form.is_valid():
            old_user = request.user
            password = old_user.password
            new_username = user_change_form.cleaned_data['new_username']
            old_user.username = hashlib.md5(new_username).hexdigest()[2:]
            old_user.email = new_username
            new_user = old_user.save()
            logout(request)
            user = authenticate(username=new_username, password=password)
            if user is not None:
                login(request, user)
            return redirect(reverse('index'))
    else:
        user_change_form = ChangeUserForm(None)
    if 'update_password' in request.POST:
        password_change_form = PasswordChangeForm(request.user, request.POST)
        if password_change_form.is_valid():
            user = password_change_form.save()
            return redirect(reverse('index'))
    else:
        password_change_form = PasswordChangeForm(request.user, None)

    cur_date = date.today()
    stats_date = (cur_date - timedelta(days=cur_date.day)).strftime('%Y-%m-%d')
    stats_month = (cur_date - timedelta(days=cur_date.day)).strftime('%B')
    purchased_numbers_count = Numbers.objects.extra(where={
    "DATE_PURCHASED >= DATE_SUB('{0}', INTERVAL duration DAY) and DATE_PURCHASED <= '{0}'".format(stats_date),
    "forward_data!='TROPO'", "client_id!=0", "provider='TTC'"}).values('duration').annotate(
        count=Count('duration')).order_by('duration')
    args.update(csrf(request))
    args['master'] = node
    args['purchased_numbers'] = purchased_numbers_count
    args['loop_times'] = [i for i in range(len(purchased_numbers_count))]
    args['stats_month'] = stats_month
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    return render_to_response('linecommand/index.html', args,
                              context_instance=RequestContext(request))


def ClientList(request,
               template_name='linecommand/client_list.html'):
    #node = get_object_or_404(Clients, user=request.user)
    node = Clients.objects.get(user=request.user)
    client_display = node.get_children().order_by('-id')
    args = {'master': node}
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))



@login_required(login_url="/login/")
def NumbersImport(request):
    if not request.user.is_superuser:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'index'))
    if request.POST:
        number_import_form = ImportNumbers(request.POST, request.FILES)
        if number_import_form.is_valid:
            upload_dir = date.today().strftime(settings.UPLOAD_PATH)
            upload_full_path = os.path.join(settings.MEDIA_ROOT, upload_dir)
            if not os.path.exists(upload_full_path):
                os.makedirs(upload_full_path)
            file_uploaded = request.FILES['file']
            if os.path.exists(os.path.join(upload_full_path, file_uploaded.name)):
                file_uploaded.name = '_' + file_uploaded.name
            with open(os.path.join(upload_full_path, file_uploaded.name), 'wb') as fd:
                for chunk in file_uploaded.chunks():
                    fd.write(chunk)
            with open(os.path.join(upload_full_path, file_uploaded.name), 'rb') as fd:
                for row in csv.reader(fd, delimiter=','):
                    if not len(row[0]) > 9:
                        continue
                    numbers = row[0].split('-')
                    if len(numbers) == 1:
                        new_number = Numbers.objects.create(number=numbers[0], client=Clients.objects.get(pk=1))
                        new_number.save()
            return redirect(reverse('index'))
    else:
        number_import_form = ImportNumbers(None)
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args = {}
    args.update(csrf(request))
    args['form'] = number_import_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    return render_to_response('linecommand/importnumbers.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Create(request):
    client_form = CreateClient(request.POST or None)
    args = {}
    if client_form.is_valid():
        check = User.objects.filter(email=client_form.cleaned_data['email'])
        if check.count() == 0:
            username = hashlib.md5(client_form.cleaned_data['email']).hexdigest()[2:]
            user_new = User.objects.create_user(username=username, email=client_form.cleaned_data['email'],
                                                password=client_form.cleaned_data['password'])
            # user_new.save()
            discount = (100 - client_form.cleaned_data['discount'])/100
            client = Clients.objects.create(landline_30=client_form.cleaned_data['price_30'],
                                            landline_60=client_form.cleaned_data['price_60'],
                                            landline_90=client_form.cleaned_data['price_90'],
                                            geo_landline_30=client_form.cleaned_data['geo30_price'],
                                            geo_landline_90=client_form.cleaned_data['geo90_price'],
                                            noncl_landline=client_form.cleaned_data['noncl_price'],
                                            discount_factor=discount,
                                            binded=0,
                                            active=client_form.cleaned_data['active'], user=user_new,
                                            parent=Clients.objects.get(user=request.user.id))
            client.save()
            #return HttpResponse("Successfully Created Client!")
            return redirect(reverse("index"))
        else:
            args['create_error'] = "Email already exists."
    args.update(csrf(request))
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client_form'] = client_form
    return render_to_response('linecommand/createclient.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Deposit(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants()
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
    form = DepositForm(client, request.POST or None)
    if form.is_valid():
        amount = form.cleaned_data['amount']
        if amount >= 0:
            trans_type = 'Balance added by %s' % request.user.email
        else:
            trans_type = 'Balance deducted by %s' % request.user.email

        client_form = get_object_or_404(Clients, pk=form.cleaned_data['client'])
        # deposit_form = form.save(commit=False)
        # deposit_form.client = client
        deposit_amount = Accounts.objects.create(
            client=client_form,
            amount=amount,
            transaction_type=trans_type,
            trx_type=1
        )
        subject = 'Client Funding'
        message = render_to_string('linecommand/funding_email.html', {'client': client[0],
                                                                      'amount': amount,
                                                                      'site_name': 'LineCommand'
                                                                      })
        to_emails = [client[0].parent.user.email]
        from_email = 'support@voiceoverlines.com'
        s = send_mail(subject, message, from_email, to_emails)
        # deposit_amount.save()
        return redirect(reverse("index"))
    args = {}
    args.update(csrf(request))
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['form'] = form
    args['client'] = client[0]
    return render_to_response('linecommand/deposit.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def SetPrice(request, client_id):
    client = get_object_or_404(Clients, pk=client_id)
    children_list = Clients.objects.get(user=request.user).get_descendants()
    if client not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
    if 'change_price' in request.POST:
        change_price_form = ChangePrice(request.POST)
        if change_price_form.is_valid():
            if not client.id == change_price_form.cleaned_data['client']:
                return HttpResponse('''<br><h3>You don't have permission to modify request client.\
                                    </h3><br><a href="%s">GoBack</a>''' % reverse('modify', args=(client.id,)))
                # newPrice = change_price_form.cleaned_data['price']
            discount = (100 - change_price_form.cleaned_data['discount'])/100
            client_update = Clients.objects.filter(pk=change_price_form.cleaned_data['client']).update(
                landline_30=change_price_form.cleaned_data['price_30'],
                landline_60=change_price_form.cleaned_data['price_60'],
                landline_90=change_price_form.cleaned_data['price_90'],
                geo_landline_30=change_price_form.cleaned_data['geo_30'],
                geo_landline_90=change_price_form.cleaned_data['geo_90'],
                noncl_landline=change_price_form.cleaned_data['noncl_price'],
                discount_factor=discount
            )
            return redirect(reverse("index"))
    else:
        change_price_form = ChangePrice(initial={
            'client': client.id,
            'price_30': client.landline_30,
            'price_60': client.landline_60,
            'price_90': client.landline_90,
            'geo_30': client.geo_landline_30,
            'geo_90': client.geo_landline_90,
            'discount': client.percentage_discount
        })
    if 'change_username' in request.POST:
        user_change_form = ChangeUserForm(request.POST)
        if user_change_form.is_valid():
            old_user = client.user
            password = old_user.password
            new_username = user_change_form.cleaned_data['new_username']
            old_user.username = hashlib.md5(new_username).hexdigest()[2:]
            old_user.email = new_username
            new_user = old_user.save()
            return redirect(reverse("index"))
    else:
        user_change_form = ChangeUserForm(None)
    if 'change_password' in request.POST:
        password_set_form = SetPasswordForm(client.user, request.POST)
        if password_set_form.is_valid():
            user = password_set_form.save()
            return redirect(reverse("index"))
    else:
        password_set_form = SetPasswordForm(client.user, None)
    args = {}
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args.update(csrf(request))
    args['price_form'] = change_price_form
    args['username_form'] = user_change_form
    args['password_form'] = password_set_form
    args['client'] = client
    return render_to_response('linecommand/modify.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def PurchaseNumbers(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if not client.count() > 0:
        raise Http404
    #num_aggregate = Clients.objects.filter(~Q(numbers__provider = 'INETWORK'), pk=1).aggregate(count=Count('numbers'))
    num_pool = Numbers.objects.filter(~Q(provider='INETWORK'), client__id=1, deleted=0)
    numbers_available = num_pool.count()

    form = AddNumbers(client, request.POST or None)
    args = {}
    if form.is_valid():
        limit = form.cleaned_data['count']
        validate_ancestors = client[0].get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
        valid = True
        duration = form.cleaned_data['duration']
        note = form.cleaned_data['notes']
        for obj in validate_ancestors:
            if obj.user.is_superuser:
                continue
            if duration == '30':
                price = obj.landline_30
            elif duration == '60':
                price = obj.landline_60
            else:
                price = obj.landline_90
            if not obj.total_amount >= (price * limit):
                valid = False
                break
        forward_type = form.cleaned_data['forward']
        if forward_type == 'FORWARD':
            #forward_data = re.sub(r'[\s\'\"\(\)-]', '', form.cleaned_data['forward_data'])
            status, forward_data = validate_number(form.cleaned_data['forward_data'])
        else:
            #match = re.search(r'[%s]' % string.letters, form.cleaned_data['forward_data'])
            #if match:
            #    forward_data = re.sub(r'[\s\'\"\(\)]', '', form.cleaned_data['forward_data'])
            #else:
            #    forward_data = re.sub(r'[\s\'\"\(\)-]', '', form.cleaned_data['forward_data'])
            status, forward_data = validate_number(form.cleaned_data['forward_data'], False)
        if not status:
            args['errors'] = 'Invalid value for forward_data.'
            valid = False
        if valid:
            #number_to_update = Numbers.objects.filter(
            #    ~Q(provider='INETWORK'),
            #    client=Clients.objects.get(pk=1),
            #    deleted=0
            #).order_by('?')[:limit]
            number_to_update = num_pool.order_by('?')[:limit]
            duration = form.cleaned_data['duration']
            for number in number_to_update:
                number.client = client[0]
                number.forward_type = forward_type
                number.duration = duration
                number.forward_data = forward_data
                number.date_purchased = datetime.now()
                #number.date_expiry = datetime.now() + timedelta(days=int(duration))
                number.notes = note
                number.save()
            for obj in validate_ancestors:
                if obj.user.is_superuser:
                    continue
                if obj.id == client[0].id:
                    #trans_type = '%d Numbers purchased' % limit
                    trans_type = '%d Numbers purchased by %s' % (limit, request.user.email)
                else:
                    #trans_type = '%d Numbers purchased by client' % limit
                    trans_type = '%d Numbers purchased by %s for %s' % (limit, request.user.email, client[0].user.email)
                if duration == '30':
                    price = obj.landline_30
                elif duration == '60':
                    price = obj.landline_60
                else:
                    price = obj.landline_90
                transaction = Accounts.objects.create(
                    client=obj,
                    amount=-(price * limit),
                    transaction_type=trans_type
                )
                transaction.save()
            return redirect(reverse('numbers', args=(client[0].id,)))
        else:
            args['errors'] = "You don't have sufficient balance."
    numbers_client = Numbers.objects.filter(client=client[0]).order_by('-date_purchased')
    paginator = Paginator(numbers_client, 50)
    page = request.GET.get('page')
    try:
        numbers = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        numbers = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        numbers = paginator.page(paginator.num_pages)
    args.update(csrf(request))
    args['form'] = form
    args['client'] = client[0]
    args['total_numbers'] = numbers_available
    args['numbers'] = numbers
    return render_to_response('linecommand/numbers.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def ShowNumbers(request, client_id):
    client = get_object_or_404(Clients, pk=client_id)
    orderby = request.GET.get('order_by', 'date')
    items = int(request.GET.get('items', '20'))
    if orderby == 'num':
        numbers_client = Numbers.objects.filter(client=client).order_by('-number')
    elif orderby == 'frwd':
        numbers_client = Numbers.objects.filter(client=client).order_by('-forward_data')
    else:
        numbers_client = Numbers.objects.filter(client=client).order_by('-date_purchased')
    paginator = Paginator(numbers_client, items)
    page = request.GET.get('page')
    try:
        numbers = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        numbers = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        numbers = paginator.page(paginator.num_pages)
    args = {}
    args['numbers'] = numbers
    args['order_by'] = orderby
    args['items'] = items
    return render_to_response('linecommand/shownumbers.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Details(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    args = {}
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="/">GoBack</a>''')
    if 'add_balance' in request.POST:
        balance_form = BalanceForm(client, request.POST)
        if balance_form.is_valid():
            client_form = get_object_or_404(Clients, pk=balance_form.cleaned_data['client'])
            deposit_amount = Accounts.objects.create(client=client_form, amount=balance_form.cleaned_data['amount'])
            return redirect(reverse('detail', args=(client[0].id,)))
    else:
        balance_form = BalanceForm(client)
    if 'update_numbers' in request.POST:
        update_form = UpdateNumbersForm(request.POST)
        if update_form.is_valid():
            numbers_id = request.POST.getlist('update_id')
            forward_type = update_form.cleaned_data['forward']
            if forward_type == 'FORWARD':
                forward_data = re.sub(r'[\s\'\"\(\)-]', '', update_form.cleaned_data['forward_data'])
            else:
                match = re.search(r'[%s]' % string.letters, update_form.cleaned_data['forward_data'])
                if match:
                    forward_data = re.sub(r'[\s\'\"\(\)]', '', update_form.cleaned_data['forward_data'])
                else:
                    forward_data = re.sub(r'[\s\'\"\(\)-]', '', update_form.cleaned_data['forward_data'])
            update_status = Numbers.objects.filter(id__in=numbers_id).update(forward_type=forward_type,
                                                                             forward_data=forward_data)
            return redirect(reverse('detail', args=(client[0].id,)))
    else:
        update_form = UpdateNumbersForm(None)

    num_aggregate = Clients.objects.filter(pk=1).aggregate(count=Count('numbers'))
    numbers_available = num_aggregate.get('count', 0)
    if 'buy_numbers' in request.POST:
        number_form = AddNumbers(client, request.POST)
        if number_form.is_valid():
            limit = number_form.cleaned_data['count']
            limit_valid = True
            if not limit <= numbers_available:
                args['numbers_errors'] = ["Your request for numbers exceeds available numbers"]
                limit_valid = False
            validate_ancestors = client[0].get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
            balance_valid = True
            for obj in validate_ancestors:
                if obj.user.is_superuser:
                    continue
                if not obj.total_amount >= (obj.price * limit):
                    balance_valid = False
                    args['numbers_errors'] = ["You don't have sufficient balance."]
                    break
            if limit_valid and balance_valid:
                # if form.cleaned_data['order'] == 'Last_Added':
                #     number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1))[:limit]
                # elif form.cleaned_data['order'] == 'Random':
                #     number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1)).order_by('?')[:limit]
                number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1)).order_by('?')[:limit]
                forward_type = number_form.cleaned_data['forward']
                if forward_type == 'FORWARD':
                    forward_data = re.sub(r'[\s\'\"\(\)-]', '', number_form.cleaned_data['forward_data'])
                else:
                    match = re.search(r'[%s]' % string.letters, number_form.cleaned_data['forward_data'])
                    if match:
                        forward_data = re.sub(r'[\s\'\"\(\)]', '', number_form.cleaned_data['forward_data'])
                    else:
                        forward_data = re.sub(r'[\s\'\"\(\)-]', '', number_form.cleaned_data['forward_data'])
                for number in number_to_update:
                    number.client = client[0]
                    number.forward_type = forward_type
                    number.duration = number_form.cleaned_data['duration']
                    number.forward_data = forward_data
                    number.date_purchased = datetime.now()
                    #number.date_expiry = datetime.now() + timedelta(days=int(number_form.cleaned_data['duration']))
                    number.save()
                for obj in validate_ancestors:
                    if obj.user.is_superuser:
                        continue
                    transaction = Accounts.objects.create(client=obj, amount=-(obj.price * limit))
                    transaction.save()
                return redirect(reverse('detail', args=(client[0].id,)))
    else:
        number_form = AddNumbers(client)
    if 'create_client' in request.POST:
        client_form = CreateClient(request.POST)
        if client_form.is_valid():
            username = hashlib.md5(client_form.cleaned_data['email']).hexdigest()[:30]
            user_new = User.objects.create_user(username=username, email=client_form.cleaned_data['email'],
                                                password=client_form.cleaned_data['password'])
            # user_new.save()
            new_client = Clients.objects.create(price=client_form.cleaned_data['price'], binded=0,
                                                active=client_form.cleaned_data['active'],
                                                user=user_new, parent=client[0])
            new_client.save()
            #return HttpResponse("Successfully Created Client!")
            return redirect(reverse("detail", args=(new_client.id,)))
    else:
        client_form = CreateClient(None)
    if 'update_username' in request.POST:
        user_change_form = ChangeUserForm(request.POST)
        if user_change_form.is_valid():
            old_user = request.user
            password = old_user.password
            new_username = user_change_form.cleaned_data['new_username']
            old_user.username = hashlib.md5(new_username).hexdigest()[:30]
            old_user.email = new_username
            new_user = old_user.save()
            logout(request)
            user = authenticate(username=new_username, password=password)
            if user is not None:
                login(request, user)
            return redirect(reverse('detail', args=(client[0].id,)))
    else:
        user_change_form = ChangeUserForm(None)
    if 'update_password' in request.POST:
        password_change_form = PasswordChangeForm(request.user, request.POST)
        if password_change_form.is_valid():
            user = password_change_form.save()
            return redirect(reverse('detail', args=(client[0].id,)))
    else:
        password_change_form = PasswordChangeForm(request.user, None)

    numbers_client = Numbers.objects.filter(client=client).order_by('-date_purchased')
    current_balance = client.annotate(total=Sum('account__amount'))[0].total
    args.update(csrf(request))
    args['balance_form'] = balance_form
    args['balance'] = current_balance
    args['number_form'] = number_form
    args['update_number_form'] = update_form
    args['client_form'] = client_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    args['total_numbers'] = numbers_available
    args['numbers'] = numbers_client
    return render_to_response('linecommand/main.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Client_main(request, client_id):
    if 'export_numbers' in request.POST:
        #return redirect(reverse('export_numbers', args=(client_id,)))
        return Client_exportNumbers(request, client_id)
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="/">GoBack</a>''')
    args = {}
    args['numbers_errors'] = []
    if 'add_balance' in request.POST:
        balance_form = BalanceForm(client, request.POST)
        if balance_form.is_valid():
            amount = balance_form.cleaned_data['amount']
            if amount >= 0:
                trans_type = 'Balance added by %s' % request.user.email
            else:
                trans_type = 'Balance deducted by %s' % request.user.email
            client_form = get_object_or_404(Clients, pk=balance_form.cleaned_data['client'])
            deposit_amount = Accounts.objects.create(
                client=client_form,
                amount=amount,
                transaction_type=trans_type,
                trx_type=1
            )
            subject = 'Client Funding'
            message = render_to_string('linecommand/funding_email.html', {'client': client[0],
                                                                          'amount': amount,
                                                                          'site_name': 'LineCommand'
                                                                          })
            to_emails = [client[0].parent.user.email]
            from_email = 'support@voiceoverlines.com'
            s = send_mail(subject, message, from_email, to_emails)
            return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        balance_form = BalanceForm(client)
    if 'update_numbers' in request.POST:
        update_form = UpdateNumbersForm(request.POST)
        if update_form.is_valid():
            numbers_id = request.POST.getlist('update_id')
            forward_type = update_form.cleaned_data['forward']
            if forward_type == 'FORWARD':
                status, forward_data = validate_number(update_form.cleaned_data['forward_data'])
            else:
                status, forward_data = validate_number(update_form.cleaned_data['forward_data'], False)
            if status:
                numbers_to_update = Numbers.objects.filter(id__in=numbers_id)
                if numbers_to_update.filter(deleted=1).count() > 0:
                    args['numbers_errors'].append('Forward information can not be changed for expired numbers.')
                for num in numbers_to_update.filter(deleted=0):
                    frwrd_log = ForwardingLogs.objects.create(
                        number=num,
                        changed_by=request.user,
                        changed_from=num.forward_data,
                        changed_to=forward_data
                    )
                    frwrd_log.save()
                update_status = numbers_to_update.filter(deleted=0).update(
                    forward_type=forward_type,
                    forward_data=forward_data
                )
                return redirect(reverse('client_main', args=(client[0].id,)))
            else:
                args['numbers_errors'].append('Invalid value for forward_data.')
    else:
        update_form = UpdateNumbersForm(None)
    if 'sms_update' in request.POST:
        sms_info_form = UpdateSMSTypeForm(request.POST)
        if sms_info_form.is_valid():
            numbers_id = request.POST.getlist('update_id')
            nums =Numbers.objects.filter(id__in=numbers_id).update(
                sms_type=sms_info_form.cleaned_data['sms_type'],
                sms_data=sms_info_form.cleaned_data['sms_data']
            )
            return redirect(reverse('client_main', args=(client[0].id,)))
        else:
            args['numbers_errors'].append('Invalid request to update SMS information.')
    else:
        sms_info_form = UpdateSMSTypeForm(None)
    if 'renew_numbers' in request.POST:
        renew_number_form = RenewNumbersForm(request.POST)
        if renew_number_form.is_valid():
            numbers_id = request.POST.getlist('update_id')
            renew_numbers = Numbers.objects.filter(id__in=numbers_id)
            numbers_count = renew_numbers.values('provider').annotate(num_count=Count('provider'))
            #for num in renew_numbers:
            #    if not num.provider == 'TTC':
            #        args['numbers_errors'].append("Only random numbers can be renewed.")
            #        break
            limit = len(numbers_id)
            duration = int(renew_number_form.cleaned_data['duration'])
            validate_ancestors = client[0].get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
            balance_valid = True

            for obj in validate_ancestors:
                if obj.user.is_superuser:
                    continue
                #if duration == 30:
                #    price = obj.landline_30
                #elif duration == 60:
                #    price = obj.landline_60
                #else:
                #    price = obj.landline_90
                total_price = get_total_price(obj, numbers_count, duration)
                #if not obj.total_amount >= (price * limit):
                if not obj.total_amount >= total_price:
                    balance_valid = False
                    args['numbers_errors'].append("You don't have sufficient balance.")
                    break
            if balance_valid:
                numbers_renewed = 0
                for num in renew_numbers:
                    #if not num.provider == 'TTC':
                    #    continue
                    if num.date_expiry < date.today():
                        date_purchased = datetime.now()
                        num.date_purchased = date_purchased
                        num.duration = duration
                        #num.date_expiry = date_purchased + timedelta(days=duration)
                    else:
                        _duration = num.duration + duration
                        num.duration = _duration
                    #    date_expiry = num.date_expiry
                    #    num.date_expiry = date_expiry + timedelta(days=duration)
                    #num.duration = duration
                    num.deleted = 0
                    num.save()
                    numbers_renewed += 1
                #update_status = Numbers.objects.filter(id__in=numbers_id).update(
                #    duration=duration,
                #    date_purchased=datetime.now(),
                #    date_expiry=datetime.now() + timedelta(days=duration),
                #    deleted=0
                #)
                if numbers_renewed > 0:
                    for obj in validate_ancestors:
                        if obj.user.is_superuser:
                            continue
                        if obj.id == client[0].id:
                            trans_type = '%d Numbers renewed by %s' % (limit, request.user.email)
                        else:
                            trans_type = '%d Numbers renewed by %s for %s' % (limit, request.user.email, client[0].user.email)
                        #if duration == 30:
                        #    price = obj.landline_30
                        #elif duration == 60:
                        #    price = obj.landline_60
                        #else:
                        #    price = obj.landline_90
                        total_price = get_total_price(obj, numbers_count, duration)
                        transaction = Accounts.objects.create(
                            client=obj,
                            amount=-total_price,
                            transaction_type=trans_type)
                        transaction.save()
                return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        renew_number_form = RenewNumbersForm(None)
    if 'update_username' in request.POST:
        user_change_form = ChangeUserForm(request.POST)
        if user_change_form.is_valid():
            old_user = request.user
            password = old_user.password
            new_username = user_change_form.cleaned_data['new_username']
            old_user.username = hashlib.md5(new_username).hexdigest()[2:]
            old_user.email = new_username
            new_user = old_user.save()
            logout(request)
            user = authenticate(username=new_username, password=password)
            if user is not None:
                login(request, user)
            return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        user_change_form = ChangeUserForm(None)
    if 'update_password' in request.POST:
        password_change_form = PasswordChangeForm(request.user, request.POST)
        if password_change_form.is_valid():
            user = password_change_form.save()
            return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        password_change_form = PasswordChangeForm(request.user, None)
    export_number_form = ExportNumbers(None)
    numbers_client = Numbers.objects.filter(client=client).order_by('-date_purchased', '?')
    current_balance = client.annotate(total=Sum('account__amount'))[0].total
    args.update(csrf(request))
    args['balance_form'] = balance_form
    args['balance'] = current_balance
    args['update_number_form'] = update_form
    args['renew_number_form'] = renew_number_form
    args['sms_info_form'] = sms_info_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['export_number_form'] = export_number_form
    args['client'] = client[0]
    args['numbers'] = numbers_client
    return render_to_response('linecommand/client_main.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Client_buyNumbers(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
    args = {}
    args['numbers_errors'] = []
    #num_aggregate = Clients.objects.filter(pk=1, ).aggregate(count=Count('numbers'))
    num_pool = Numbers.objects.filter(~Q(provider='INETWORK'), client__id=1, deleted=0)
    numbers_available = num_pool.count()
    number_form = AddNumbers(client, request.POST or None)
    if number_form.is_valid():
        limit = number_form.cleaned_data['count']
        limit_valid = True
        if not limit <= numbers_available:
            args['numbers_errors'].append("Your request for numbers exceeds available numbers")
            limit_valid = False
        validate_ancestors = client[0].get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
        balance_valid = True
        duration = number_form.cleaned_data['duration']
        note = number_form.cleaned_data['notes']
        for obj in validate_ancestors:
            if obj.user.is_superuser:
                continue
            if duration == '30':
                price = obj.landline_30
            elif duration == '60':
                price = obj.landline_60
            else:
                price = obj.landline_90
            if not obj.total_amount >= (price * limit):
                balance_valid = False
                args['numbers_errors'].append("You don't have sufficient balance.")
                break
        forward_type = number_form.cleaned_data['forward']
        if forward_type == 'FORWARD':
            #forward_data = re.sub(r'[\s\'\"\(\)-]', '', number_form.cleaned_data['forward_data'])
            status, forward_data = validate_number(number_form.cleaned_data['forward_data'])
        else:
            #match = re.search(r'[%s]' % string.letters, number_form.cleaned_data['forward_data'])
            #if match:
            #    forward_data = re.sub(r'[\s\'\"\(\)]', '', number_form.cleaned_data['forward_data'])
            #else:
            #    forward_data = re.sub(r'[\s\'\"\(\)-]', '', number_form.cleaned_data['forward_data'])
            status, forward_data = validate_number(number_form.cleaned_data['forward_data'], False)
        if not status:
            balance_valid = False
            args['numbers_errors'].append('Invalid value for forward_data.')
        if limit_valid and balance_valid:
            # if form.cleaned_data['order'] == 'Last_Added':
            #     number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1))[:limit]
            # elif form.cleaned_data['order'] == 'Random':
            #     number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1)).order_by('?')[:limit]
            #if limit > 1:
            #    first_half = limit//2
            #    second_half = first_half + (limit % 2)
            #    n1 = Numbers.objects.filter(
            #        client=Clients.objects.get(pk=1),
            #        deleted=0,
            #        provider='TTC'
            #    ).order_by('?')[:first_half]
            #    n2 = Numbers.objects.filter(
            #        client=Clients.objects.get(pk=1),
            #        provider='MICHIGAN',
            #        deleted=0
            #    ).order_by('?')[:second_half]
            #    number_to_update = list(chain(n1, n2))
            #else:
            #    number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1)).order_by('?')[:limit]
            #number_to_update = Numbers.objects.filter(
            #    ~Q(provider='INETWORK'),
            #    client=Clients.objects.get(pk=1),
            #    deleted=0
            #).order_by('?')[:limit]
            number_to_update = num_pool.order_by('?')[:limit]
            for number in number_to_update:
                number.client = client[0]
                number.forward_type = forward_type
                number.duration = duration
                number.forward_data = forward_data
                number.date_purchased = datetime.now()
                #number.date_expiry = datetime.now() + timedelta(days=int(duration))
                number.notes = note
                number.save()
            for obj in validate_ancestors:
                if obj.user.is_superuser:
                    continue
                if obj.id == client[0].id:
                    trans_type = '%d Numbers purchased by %s' % (limit, request.user.email)
                else:
                    trans_type = '%d Numbers purchased by %s for %s' % (limit, request.user.email, client[0].user.email)
                if duration == '30':
                    price = obj.landline_30
                elif duration == '60':
                    price = obj.landline_60
                else:
                    price = obj.landline_90
                transaction = Accounts.objects.create(
                    client=obj,
                    amount=-(price * limit),
                    transaction_type=trans_type
                )
                transaction.save()
            return redirect(reverse('client_main', args=(client[0].id,)))

    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)

    args.update(csrf(request))
    args['number_form'] = number_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response('linecommand/client_buynumbers.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Client_sms_management(request, client_id,
                          template_name='linecommand/client_sms_management.html'):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="/">GoBack</a>''')
    args = {}
    args['numbers_errors'] = []
    if 'sms_update' in request.POST:
        sms_info_form = UpdateSMSTypeForm(request.POST)
        if sms_info_form.is_valid():
            numbers_id = request.POST.getlist('update_id')
            nums =Numbers.objects.filter(id__in=numbers_id).update(
                sms_type=sms_info_form.cleaned_data['sms_type'],
                sms_data=sms_info_form.cleaned_data['sms_data']
            )
            return redirect(reverse('client_sms', args=(client[0].id,)))
        else:
            args['numbers_errors'].append('Invalid request to update SMS information.')
    else:
        sms_info_form = UpdateSMSTypeForm(None)
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    numbers_client = Numbers.objects.filter(client=client, provider='INETWORK').order_by('-date_purchased', '?')
    args.update(csrf(request))
    args['sms_info_form'] = sms_info_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    args['numbers'] = numbers_client
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Client_createClient(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    args = {}
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
    args = {}
    client_form = CreateClient(request.POST or None)
    if client_form.is_valid():
        check = User.objects.filter(email=client_form.cleaned_data['email'])
        if check.count() == 0:
            username = hashlib.md5(client_form.cleaned_data['email']).hexdigest()[2:]
            user_new = User.objects.create_user(username=username, email=client_form.cleaned_data['email'],
                                                password=client_form.cleaned_data['password'])
            # user_new.save()
            discount = (100 - client_form.cleaned_data['discount'])/100
            new_client = Clients.objects.create(landline_30=client_form.cleaned_data['price_30'],
                                                landline_60=client_form.cleaned_data['price_60'],
                                                landline_90=client_form.cleaned_data['price_90'],
                                                geo_landline_30=client_form.cleaned_data['geo30_price'],
                                                geo_landline_90=client_form.cleaned_data['geo90_price'],
                                                noncl_landline=client_form.cleaned_data['noncl_price'],
                                                discount_factor=discount,
                                                binded=0, active=client_form.cleaned_data['active'],
                                                user=user_new, parent=client[0])
            new_client.save()
            #return HttpResponse("Successfully Created Client!")
            return redirect(reverse("client_main", args=(new_client.id,)))
        else:
            args['create_error'] = "Email already exists."

    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)

    args.update(csrf(request))
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    args['client_form'] = client_form
    return render_to_response('linecommand/client_createclient.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Client_exportNumbers(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if request.POST:
        if client.count() == 0:
            raise Http404
        children_list = Clients.objects.get(user=request.user).get_descendants(True)

        if client[0] not in children_list:
            return HttpResponse(
                '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
        args = {}
        form = ExportNumbers(request.POST)
        if form.is_valid():
            numbers_id = request.POST.getlist('update_id')
            if form.cleaned_data['export'] == '2':
                numbers_client = Numbers.objects.filter(client=client[0], id__in=numbers_id).order_by('?')
            elif form.cleaned_data['export'] == '1':
                numbers_client = Numbers.objects.filter(client=client[0]).order_by('?')
            elif form.cleaned_data['export'] == '0':
                #numbers_client = Numbers.objects.filter(client=client).filter(date_purchased=date.today()).order_by(
                #    '-date_purchased')
                search_date = datetime.now().strftime('%Y-%m-%d')
                numbers_client = Numbers.objects.filter(
                    client=client
                ).extra(
                    where={
                        "date(date_purchased)=CURDATE()"
                    }
                ).order_by('?')
            output = '<pre>\n'
            for obj in numbers_client:
                output += obj.number + '\n'
            output += '</pre>'
            return HttpResponse(output)
        else:
            return redirect(reverse("client_main", args=(client[0].id,)))
    else:
        return redirect(reverse("client_main", args=(client[0].id,)))


@login_required(login_url="/login/")
def Client_randomNumbers(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
    args = {}
    args['numbers_errors'] = []
    #num_aggregate = Clients.objects.filter(pk=1).aggregate(count=Count('numbers'))
    num_pool = Numbers.objects.filter(~Q(provider='INETWORK'), client__id=1, deleted=0)
    numbers_available = num_pool.count()

    query = request.REQUEST.get('type')
    if request.POST:
        number_form = RandomNumbersForm(request.POST)
        if number_form.is_valid():
            limit = number_form.cleaned_data['count']
            limit_valid = True
            if not limit <= numbers_available:
                args['numbers_errors'].append("Your request for numbers exceeds available numbers")
                limit_valid = False
            validate_ancestors = client[0].get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
            balance_valid = True
            duration = number_form.cleaned_data['duration']
            note = number_form.cleaned_data['notes']
            for obj in validate_ancestors:
                if obj.user.is_superuser:
                    continue
                if duration == 30:
                    price = obj.landline_30
                elif duration == 60:
                    price = obj.landline_60
                else:
                    price = obj.landline_90
                if not obj.total_amount >= (price * limit):
                    balance_valid = False
                    args['numbers_errors'].append("You don't have sufficient balance.")
                    break
            forward_type = number_form.cleaned_data['forward']
            if forward_type == 'FORWARD':
                #forward_data = re.sub(r'[\s\'\"\(\)-]', '', number_form.cleaned_data['forward_data'])
                status, forward_data = validate_number(number_form.cleaned_data['forward_data'])
            else:
                #match = re.search(r'[%s]' % string.letters, number_form.cleaned_data['forward_data'])
                #if match:
                #    forward_data = re.sub(r'[\s\'\"\(\)]', '', number_form.cleaned_data['forward_data'])
                #else:
                #    forward_data = re.sub(r'[\s\'\"\(\)-]', '', number_form.cleaned_data['forward_data'])
                status, forward_data = validate_number(number_form.cleaned_data['forward_data'], False)
            if not status:
                balance_valid = False
                args['numbers_errors'].append('Invalid value for forward_data.')
            if limit_valid and balance_valid:
                # if form.cleaned_data['order'] == 'Last_Added':
                #     number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1))[:limit]
                # elif form.cleaned_data['order'] == 'Random':
                #     number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1)).order_by('?')[:limit]
                #if limit > 1:
                #    first_half = limit//2
                #    second_half = first_half + (limit % 2)
                #    n1 = Numbers.objects.filter(
                #        client=Clients.objects.get(pk=1),
                #        provider='TTC',
                #        deleted=0
                #    ).order_by('?')[:first_half]
                #    n2 = Numbers.objects.filter(
                #        client=Clients.objects.get(pk=1),
                #        provider='MICHIGAN',
                #        deleted=0
                #    ).order_by('?')[:second_half]
                #    number_to_update = list(chain(n1, n2))
                #else:
                #    number_to_update = Numbers.objects.filter(client=Clients.objects.get(pk=1)).order_by('?')[:limit]
                #number_to_update = Numbers.objects.filter(
                #    ~Q(provider='INETWORK'),
                #    client=Clients.objects.get(pk=1),
                #    deleted=0
                #).order_by('?')[:limit]
                number_to_update = num_pool.order_by('?')[:limit]
                for number in number_to_update:
                    number.client = client[0]
                    number.forward_type = forward_type
                    number.duration = duration
                    number.forward_data = forward_data
                    number.date_purchased = datetime.now()
                    #number.date_expiry = datetime.now() + timedelta(days=duration)
                    number.notes = note
                    number.save()
                for obj in validate_ancestors:
                    if obj.user.is_superuser:
                        continue
                    if obj.id == client[0].id:
                        trans_type = '%d Numbers purchased by %s' % (limit, request.user.email)
                    else:
                        trans_type = '%d Numbers purchased by %s for %s' % (limit, request.user.email, client[0].user.email)
                    if duration == 30:
                        price = obj.landline_30
                    elif duration == 60:
                        price = obj.landline_60
                    else:
                        price = obj.landline_90
                    transaction = Accounts.objects.create(
                        client=obj,
                        amount=-(price * limit),
                        transaction_type=trans_type
                    )
                    transaction.save()
                return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        #query = request.GET.get('type')
        if query == 'rand30':
            dur = 30
        elif query == 'rand60':
            dur = 60
        else:
            dur = 90
        number_form = RandomNumbersForm(initial={'duration': dur, 'client': client[0].id})
    current_balance = client.annotate(total=Sum('account__amount'))[0].total
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['number_form'] = number_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['balance'] = current_balance
    args['type'] = query
    args['client'] = client[0]
    return render_to_response('linecommand/client_randnumbers.html', args,
                              context_instance=RequestContext(request))

@login_required(login_url="/login/")
def Client_NonclNumbers(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
    args = {}
    args['numbers_errors'] = []
    #numbers_pool = Numbers.objects.filter(
    #    deleted=1,
    #    provider='TTC'
    #).extra(where=['date(date_expiry)<date_add(curdate(), interval -7 day)'])
    numbers_pool = Numbers.objects.filter(
        deleted=1,
        provider='TTC'
    ).extra(where=['date(date_add(date_purchased, interval duration day))<date_add(curdate(), interval -7 day)'])
    numbers_available = numbers_pool.count()

    if request.POST:
        number_form = RandomNumbersForm(request.POST)
        if number_form.is_valid():
            limit = number_form.cleaned_data['count']
            limit_valid = True
            if not limit <= numbers_available:
                args['numbers_errors'].append("Your request for numbers exceeds available numbers")
                limit_valid = False
            validate_ancestors = client[0].get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
            balance_valid = True
            duration = number_form.cleaned_data['duration']
            note = number_form.cleaned_data['notes']
            for obj in validate_ancestors:
                if obj.user.is_superuser:
                    continue
                price = obj.noncl_landline
                if not obj.total_amount >= (price * limit):
                    balance_valid = False
                    args['numbers_errors'].append("You don't have sufficient balance.")
                    break
            forward_type = number_form.cleaned_data['forward']
            if forward_type == 'FORWARD':
                status, forward_data = validate_number(number_form.cleaned_data['forward_data'])
            else:
                status, forward_data = validate_number(number_form.cleaned_data['forward_data'], False)
            if not status:
                balance_valid = False
                args['numbers_errors'].append('Invalid value for forward_data.')
            if limit_valid and balance_valid:
                number_to_update = numbers_pool.order_by('?')[:limit]
                for number in number_to_update:
                    number.client = client[0]
                    number.forward_type = forward_type
                    number.duration = duration
                    number.forward_data = forward_data
                    number.date_purchased = datetime.now()
                    #number.date_expiry = datetime.now() + timedelta(days=duration)
                    number.deleted = 0
                    number.notes = note
                    number.save()
                for obj in validate_ancestors:
                    if obj.user.is_superuser:
                        continue
                    if obj.id == client[0].id:
                        trans_type = '%d Numbers purchased by %s' % (limit, request.user.email)
                    else:
                        trans_type = '%d Numbers purchased by %s for %s' % (limit, request.user.email, client[0].user.email)
                    price = obj.noncl_landline
                    transaction = Accounts.objects.create(
                        client=obj,
                        amount=-(price * limit),
                        transaction_type=trans_type
                    )
                    transaction.save()
                return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        dur = 7
        number_form = RandomNumbersForm(initial={'duration': dur, 'client': client[0].id})
    current_balance = client.annotate(total=Sum('account__amount'))[0].total
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['number_form'] = number_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['balance'] = current_balance
    args['client'] = client[0]
    return render_to_response('linecommand/client_nonclnumbers.html', args,
                              context_instance=RequestContext(request))


# Backup for geo Numbers
@login_required(login_url="/login/")
def Client_GeoNumbers_old(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    args = {}
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
        #area_code_list = AreaCodes.objects.all().order_by('-id')
    #area_code_list = Areas.objects.filter(rank__lt=101).order_by('human_readable_name')
    #states_list = Areas.objects.filter(rank__lt=101).order_by('state').values('state').distinct()
    states_list = Areas.objects.extra(where={"parent_id=''", "area_codes!=''"}).order_by('state').values(
        'state').distinct()
    geo_form = AddGeo(client, states_list, request.POST or None)
    args['numbers_errors'] = []
    if geo_form.is_valid():
        if request.POST.has_key('city'):
            area_code_id = request.POST['city']
            # area_code = geo_form.cleaned_data['area_code']
        area_code = Areas.objects.get(pk=area_code_id).area_codes
        limit = geo_form.cleaned_data['count']
        forward_type = geo_form.cleaned_data['forward']
        #forward_data = geo_form.cleaned_data['forward_data']
        duration = geo_form.cleaned_data['duration']
        validate_ancestors = client[0].get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
        balance_valid = True
        for obj in validate_ancestors:
            if obj.user.is_superuser:
                continue
            if duration == '30':
                price = obj.geo_landline_30
            else:
                price = obj.geo_landline_90
            if not obj.total_amount >= (price * limit):
                balance_valid = False
                args['numbers_errors'].append("You don't have sufficient balance.")
                break
        if balance_valid and (not area_code == ""):
            geo_purchased = []
            date_purchased = datetime.now()
            #date_expiry = datetime.now() + timedelta(days=int(duration))
            if forward_type == 'FORWARD':
                forward_data = re.sub(r'[\s\'\"\(\)-]', '', geo_form.cleaned_data['forward_data'])
            else:
                match = re.search(r'[%s]' % string.letters, geo_form.cleaned_data['forward_data'])
                if match:
                    forward_data = re.sub(r'[\s\'\"\(\)]', '', geo_form.cleaned_data['forward_data'])
                else:
                    forward_data = re.sub(r'[\s\'\"\(\)-]', '', geo_form.cleaned_data['forward_data'])
            inetworkObj = bandwidth.INetwork()
            # status, message = dids.GeoNumbers(area_code, limit, geo_purchased)
            status, message = inetworkObj.Order_Numbers(limit, area_code, geo_purchased)
            if status:
                count_purchase = len(geo_purchased)
                if count_purchase == 0:
                    args['numbers_errors'].append('Sorry, this area is temporarily out of stock')
                else:
                    redirect_flag = True
                    for num in geo_purchased:
                        n = Numbers.objects.create(number=num, client=client[0],
                                                   duration=duration, date_purchased=date_purchased,
                                                   #date_expiry=date_expiry,
                                                   forward_type=forward_type,
                                                   forward_data=forward_data, provider='INETWORK')
                        n.save()
                    if count_purchase < limit:
                        err_purchase = "Geo Numbers matched to your criteria: %d" % count_purchase
                        args['numbers_errors'].append(err_purchase)
                        redirect_flag = False
                    for obj in validate_ancestors:
                        if obj.user.is_superuser:
                            continue
                        if obj.id == client[0].id:
                            trans_type = '%d Numbers purchased by %s' % (count_purchase, request.user.email)
                        else:
                            trans_type = '%d Numbers purchased by %s for %s' % (
                                count_purchase, request.user.email, client[0].user.email
                            )
                        if duration == '30':
                            price = obj.geo_landline_30
                        else:
                            price = obj.geo_landline_90
                        transaction = Accounts.objects.create(
                            client=obj,
                            amount=-(price * count_purchase),
                            transaction_type=trans_type
                        )
                        transaction.save()
                    if redirect_flag:
                        return redirect(reverse('client_main', args=(client[0].id,)))
            else:
                args['numbers_errors'].append(message)
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['geo_form'] = geo_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response('linecommand/client_buygeo.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Client_GeoNumbers(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    args = {}
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
        #area_code_list = AreaCodes.objects.all().order_by('-id')
    #area_code_list = Areas.objects.filter(rank__lt=101).order_by('human_readable_name')
    #states_list = Areas.objects.filter(rank__lt=101).order_by('state').values('state').distinct()
    states_list = Areas.objects.extra(
        where={"parent_id=''", "area_codes!=''"}
    ).exclude(
        state__in=['AK', 'HI']
    ).order_by('state').values('state').distinct()
    geo_form = AddGeo(client, states_list, request.POST or None)
    args['numbers_errors'] = []
    if geo_form.is_valid():
        if 'city' in request.POST:
            area_code_id = request.POST['city']
            # area_code = geo_form.cleaned_data['area_code']
        area_obj = Areas.objects.get(pk=area_code_id)
        limit = geo_form.cleaned_data['count']
        forward_type = geo_form.cleaned_data['forward']
        #forward_data = geo_form.cleaned_data['forward_data']
        duration = geo_form.cleaned_data['duration']
        note = geo_form.cleaned_data['notes']
        validate_ancestors = client[0].get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
        balance_valid = True
        for obj in validate_ancestors:
            if obj.user.is_superuser:
                continue
            if duration == '30':
                price = obj.geo_landline_30
            else:
                price = obj.geo_landline_90
            if not obj.total_amount >= (price * limit):
                balance_valid = False
                args['numbers_errors'].append("You don't have sufficient balance.")
                break
        if forward_type == 'FORWARD':
            #forward_data = re.sub(r'[\s\'\"\(\)-]', '', geo_form.cleaned_data['forward_data'])
            status, forward_data = validate_number(geo_form.cleaned_data['forward_data'])
        else:
            #match = re.search(r'[%s]' % string.letters, geo_form.cleaned_data['forward_data'])
            #if match:
            #    forward_data = re.sub(r'[\s\'\"\(\)]', '', geo_form.cleaned_data['forward_data'])
            #else:
            #    forward_data = re.sub(r'[\s\'\"\(\)-]', '', geo_form.cleaned_data['forward_data'])
            status, forward_data = validate_number(geo_form.cleaned_data['forward_data'], False)
        if not status:
            balance_valid = False
            args['numbers_errors'].append('Invalid Value for forward_data.')
        if balance_valid and (not area_obj.area_codes == ""):
            geo_purchased = {}
            date_purchased = datetime.now()
            #date_expiry = datetime.now() + timedelta(days=int(duration))
            inetworkObj = bandwidth_v2.INetwork()
            # status, message = dids.GeoNumbers(area_code, limit, geo_purchased)
            status, pending, order_url, message = inetworkObj.Order_Numbers(limit, area_obj.area_codes, geo_purchased)
            if status:
                count_purchase = len(geo_purchased)
                if count_purchase == 0:
                    args['numbers_errors'].append('Sorry, this area is temporarily out of stock')
                else:
                    redirect_flag = True
                    if pending:
                        args['numbers_errors'].append(message)
                        new_order = GeoOrders.objects.create(client=client[0],
                                                             order_url=order_url, status=1)
                        new_order.save()
                        for num in geo_purchased:
                            n = Numbers.objects.create(number=num,
                                                       client=client[0],
                                                       tier=geo_purchased.get(num, 0),
                                                       duration=duration,
                                                       date_purchased=date_purchased,
                                                       #date_expiry=date_expiry,
                                                       forward_type=forward_type,
                                                       forward_data=forward_data,
                                                       provider='INETWORK',
                                                       geo_status=new_order.id,
                                                       area=area_obj,
                                                       notes=note)
                            n.save()
                    else:
                        for num in geo_purchased:
                            n = Numbers.objects.create(number=num, client=client[0],
                                                       tier=geo_purchased.get(num, 0),
                                                       duration=duration,
                                                       date_purchased=date_purchased,
                                                       #date_expiry=date_expiry,
                                                       forward_type=forward_type,
                                                       forward_data=forward_data,
                                                       provider='INETWORK',
                                                       area=area_obj,
                                                       notes=note)
                            n.save()
                    if count_purchase < limit:
                        err_purchase = "Geo Numbers matched to your criteria: %d" % count_purchase
                        args['numbers_errors'].append(err_purchase)
                        redirect_flag = False
                    for obj in validate_ancestors:
                        if obj.user.is_superuser:
                            continue
                        if obj.id == client[0].id:
                            trans_type = '%d Numbers purchased by %s' % (count_purchase, request.user.email)
                        else:
                            trans_type = '%d Numbers purchased by %s for %s' % (
                                count_purchase, request.user.email, client[0].user.email
                            )
                        if duration == '30':
                            price = obj.geo_landline_30
                        else:
                            price = obj.geo_landline_90
                        transaction = Accounts.objects.create(
                            client=obj,
                            amount=-(price * count_purchase),
                            transaction_type=trans_type
                        )
                        transaction.save()
                    if redirect_flag:
                        return redirect(reverse('client_main', args=(client[0].id,)))
            else:
                args['numbers_errors'].append(message)
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['geo_form'] = geo_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response('linecommand/client_buygeo.html', args,
                              context_instance=RequestContext(request))


#@login_required(login_url="/login/")
#def Client_editCode(request, client_id):
#    client = Clients.objects.filter(pk=client_id)
#    if client.count() == 0:
#        raise Http404
#    if not client[0].user.id == request.user.id:
#        return HttpResponse(
#            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="{% url 'index' %}">GoBack</a>''')
#    if request.POST:
#        html_code_form = HtmlCode(request.POST)
#        if html_code_form.is_valid():
#            user_html_set = UserHtml.objects.filter(client=client[0])
#            if user_html_set.count() > 0:
#                user_html = user_html_set[0]
#            else:
#                user_html = UserHtml.objects.create(client=client[0])
#            user_html.store_code(html_code_form.cleaned_data['html'])
#            user_html.save()
#            return redirect(reverse('client_main', args=(client[0].id,)))
#    else:
#        user_html = UserHtml.objects.filter(client=client[0])
#        if not user_html.count() == 0:
#            html_code_form = HtmlCode(initial={'client': client[0].id, 'html': user_html[0].get_code()})
#        else:
#            html_code_form = HtmlCode(initial={'client': client[0].id})
#    args = {}
#    user_change_form = ChangeUserForm(None)
#    password_change_form = PasswordChangeForm(request.user, None)
#    args.update(csrf(request))
#    args['change_user_form'] = user_change_form
#    args['change_password_form'] = password_change_form
#    args['form'] = html_code_form
#    args['client'] = client[0]
#    return render_to_response('linecommand/client_editCode.html', args,
#                              context_instance=RequestContext(request))

@login_required(login_url="/login/")
def Client_History(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'client_main', args=(client[0].id,)))
    history = Accounts.objects.filter(client=client[0]).order_by('-date_transaction')
    args = {}
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['acc_history'] = history
    args['client'] = client[0]
    return render_to_response('linecommand/client_purchasehistory.html', args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Client_Funding(request, client_id,
                   template_name='linecommand/funding_main.html'):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'client_main', args=(client[0].id,)))
    history = Accounts.objects.filter(
        client__parent=client[0]
    ).extra(
        where=['type!=0']
    ).order_by('-date_transaction')
    args = {}
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['acc_history'] = history
    args['client'] = client[0]
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))


@login_required(login_url="/login/")
def Client_Referal(request, client_id,
                   template_name='linecommand/client_referal.html'):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'client_main', args=(client[0].id,)))
    args = {}
    uidb36 = int_to_base36(client[0].id)
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['uidb36'] = uidb36
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))

@login_required(login_url='/login/')
def Client_Settings(request, client_id,
                    template_name='linecommand/settings.html'):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'client_main', args=(client[0].id,)))
    args = {}
    if 'update_paypal' in request.POST:
        paypal_api_form = paypal_api(request.POST)
        if paypal_api_form.is_valid():
            if client[0].id == int(paypal_api_form.cleaned_data['client']):
                update_client = client.update(
                    paypal_email=paypal_api_form.cleaned_data['paypal_email']
                )
                return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        paypal_api_form = paypal_api(initial={'client': client[0].id, 'paypal_email': client[0].paypal_email})
    if 'update_username' in request.POST:
        user_change_form = ChangeUserForm(request.POST)
        if user_change_form.is_valid():
            old_user = client[0].user
            password = old_user.password
            new_username = user_change_form.cleaned_data['new_username']
            old_user.username = hashlib.md5(new_username).hexdigest()[2:]
            old_user.email = new_username
            new_user = old_user.save()
            return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        user_change_form = ChangeUserForm(None)
    if 'update_password' in request.POST:
        password_change_form = PasswordChangeForm(client[0].user, request.POST)
        if password_change_form.is_valid():
            user = password_change_form.save()
            return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        password_change_form = PasswordChangeForm(client[0].user, None)
    if 'html_code' in request.POST:
        html_code_form = HtmlCode(request.POST)
        if html_code_form.is_valid():
            user_html_set = UserHtml.objects.filter(client=client[0])
            if user_html_set.count() > 0:
                user_html = user_html_set[0]
            else:
                user_html = UserHtml.objects.create(client=client[0])
            user_html.store_code(html_code_form.cleaned_data['html'])
            user_html.save()
            return redirect(reverse('client_main', args=(client[0].id,)))
    else:
        user_html = UserHtml.objects.filter(client=client[0])
        if not user_html.count() == 0:
            html_code_form = HtmlCode(initial={'client': client[0].id, 'html': user_html[0].get_code()})
        else:
            html_code_form = HtmlCode(initial={'client': client[0].id})

    args.update(csrf(request))
    args['paypal_form'] = paypal_api_form
    args['html_form'] = html_code_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))

@login_required(login_url='/login/')
def VideoTutorial(request, client_id,
                   template_name='linecommand/video_tutorial.html'):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'client_main', args=(client[0].id,)))
    args = {}
    user_html = UserHtml.objects.filter(client=client[0].parent)
    if user_html.count() > 0:
        args['custom_html'] = user_html[0].get_code()
    else:
        args['custom_html'] = ''
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))


@login_required(login_url='/login/')
def client_cdr(request, client_id,
        template_name='linecommand/cdr.html'):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'client_main', args=(client[0].id,)))
    args = {}
    query_numbers = '''SELECT n.number AS number, count(*) AS count FROM NUMBERS AS n
                    INNER JOIN cdr ON n.number=cdr.destination_number
                    AND n.client_id={} GROUP BY n.number;'''.format(client[0].id)
    cursor = connection.cursor()
    cursor.execute(query_numbers)
    cdr_numbers = dictfetchall(cursor)
    cursor.close()

    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['numbers'] = cdr_numbers
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))


@login_required(login_url='/login/')
def client_cdr_detail(request, client_id, num,
                      template_name='linecommand/cdr_detail.html'):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to view requested client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'client_main', args=(client[0].id,)))
    args = {}
    query_numbers = '''SELECT cdr.caller_id_number AS caller_id, cdr.start_stamp AS start_stamp,
                    cdr.duration AS duration FROM cdr INNER JOIN NUMBERS AS n ON n.number=cdr.destination_number
                    AND n.client_id={} AND n.number={};'''.format(client[0].id, num)
    cursor = connection.cursor()
    cursor.execute(query_numbers)
    cdr_number_detail = dictfetchall(cursor)
    cursor.close()

    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['number'] = num
    args['number_detail'] = cdr_number_detail
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))

def AreaCodes(request, state):
    area_codes_list = Areas.objects.extra(where={"parent_id=''", "area_codes!=''"}).filter(state=state).order_by(
        'human_readable_name')
    args = {'area_codes_list': area_codes_list}
    return render_to_response('linecommand/areacodes.html', args,
                              context_instance=RequestContext(request))
    #return render('linecommand/areacodes.html', {'areaa_codes_list': area_codes_list})


def Maintenance(request, client_id):
    client = Clients.objects.filter(pk=client_id)
    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args = {}
    args.update(csrf(request))
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response('linecommand/maintenance.html', args,
                              context_instance=RequestContext(request))


def id_generator(size=127, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


@login_required(login_url='/login/')
def paypal(request, client_id,
           template_name='linecommand/paypal.html'):
    client = Clients.objects.filter(pk=client_id)
    if client.count() == 0:
        raise Http404
    children_list = Clients.objects.get(user=request.user).get_descendants(True)
    if client[0] not in children_list:
        return HttpResponse(
            '''<br><h3>You don't have permission to modify request client.</h3><br><a href="%s">GoBack</a>''' % reverse(
                'client_main', args=(client[0].id,)))

    # What you want the button to do.
    args = {}
    if request.POST:
        custom_args = {}
        fund_account_form = fund_account(request.POST)
        if fund_account_form.is_valid():
            amount = fund_account_form.cleaned_data['amount']
            _parent = client[0].parent
            paypal_dict = {
                "business": _parent.paypal_email,
                "amount": "%s" % str(amount),
                "item_name": "%s" % datetime.now().strftime("%Y%m%d-%H%M%S-") + str(client[0].id),
                "item_number": datetime.now().strftime("%Y%m%d-%H%M%S-") + int_to_base36(client[0].id),
                "invoice": "%s" % id_generator(),
                "custom": "%s" % (str(client_id)+'##'+_parent.paypal_email),
                "notify_url": "%s%s" % (settings.SITE_NAME, reverse('paypal-ipn')),
                "return_url": "http://beta.linecommand.com/%s/paypal/success/" % client_id,
                "cancel_return": "http://beta.linecommand.com/%s/details/" % client_id,
            }
            # Create the instance.
            form = PayPalPaymentsForm(initial=paypal_dict)
            custom_args['form'] = form.render()
            return render_to_response(
                'linecommand/paypalbutton.html',
                custom_args,
                context_instance=RequestContext(request)
            )
        else:
            custom_args['fund_form'] = fund_account_form
            custom_args['client'] = client[0]
            custom_args.update(csrf(request))
            return render_to_response(
                'linecommand/paypal_form.html',
                custom_args,
                context_instance=RequestContext(request)
            )
    else:
        fund_account_form = fund_account(initial={'client': client[0].id})

    user_change_form = ChangeUserForm(None)
    password_change_form = PasswordChangeForm(request.user, None)
    args.update(csrf(request))
    args['fund_form'] = fund_account_form
    args['change_user_form'] = user_change_form
    args['change_password_form'] = password_change_form
    args['client'] = client[0]
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))


@csrf_exempt
def paypal_success(request, client_id):
    return redirect(reverse('client_main', args=(client_id,)))


def showstats(request,
              template_name="linecommand/stats.html"):
    args = {}
    return render_to_response(template_name, args,
                              context_instance=RequestContext(request))