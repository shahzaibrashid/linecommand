from django.contrib import admin
from linecommand.models import Clients, Numbers, Accounts 

class NumbersAdmin(admin.TabularInline):
    model = Numbers
    extra = 1

class ClientsAdmin(admin.ModelAdmin):
    #list_display = ('id', 'email', 'password')
    inlines = [NumbersAdmin]

admin.site.register(Clients, ClientsAdmin)
admin.site.register(Accounts)
