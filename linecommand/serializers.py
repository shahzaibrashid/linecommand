from django.contrib.auth import authenticate

__author__ = 'shahzaib'

from django.forms import widgets
from rest_framework import serializers
from linecommand.models import Numbers, Clients, Accounts


class SnippetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clients
        #fields = ('id', 'title', 'code', 'linenos', 'language', 'style')


class customSerializer(serializers.Serializer):
    api_key = serializers.CharField(required=True)
    count = serializers.IntegerField(required=True)
    forward_type = serializers.ChoiceField(choices=(('forward', '1'), ('voicemail', '2')), required=True)
    forward_data = serializers.CharField(required=True)


class NumbersSerializer(serializers.ModelSerializer):
    date_expiry = serializers.Field(source='date_expiry')
    class Meta:
        model = Numbers
        fields = ('number', 'tier', 'date_purchased', 'date_expiry', 'forward_type', 'forward_data')


class GeoNumbersOrder(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)
    count = serializers.IntegerField(required=True)
    duration = serializers.ChoiceField(choices=((30, 30), (90, 90)), required=True)
    area_code = serializers.URLField(required=True)
    #forward_type = serializers.ChoiceField(choices=(('forward', 'forward'), ('voicemail', 'voicemail')), required=True)
    forward_type = serializers.CharField(required=True)
    forward_data = serializers.CharField(required=True)
    sms_type = serializers.CharField(required=False)
    sms_data = serializers.CharField(required=False)
    fallback = serializers.CharField(required=False)
    no_cache = serializers.CharField(required=False)

    def validate_forward_type(self, attrs, source):
        value = attrs[source]
        choices = ['FORWARD', 'VOICEMAIL']
        if not value.upper() in choices:
            raise serializers.ValidationError(
                "Select a valid choice. %s is not one of the available choices." % str(value)
            )
        return attrs

    def validate_fallback(self, attrs, source):
        if source in attrs:
            value = attrs[source]
            choices = ['TRUE', 'YES']
            if not value.upper() in choices:
                raise serializers.ValidationError(
                    "Select a valid choice. %s is not one of the available choices." % str(value)
                )
        return attrs

    def validate_sms_type(self, attrs, source):
        if source in attrs:
            value = attrs[source]
            choices = ['FORWARD', 'EMAIL', 'WEB_REQUEST']
            if not value.upper() in choices:
                raise serializers.ValidationError(
                    "Select a valid choice. %s is not one of the available choices." % str(value)
                )
        return attrs

    def validate_no_cache(selfself, attrs, source):
        if source in attrs:
            value = attrs[source]
            choices = ['YES', 'TRUE']
            if not value.upper() in choices:
                raise serializers.ValidationError(
                    "Select a valid choice. %s is not one of the available choices." % str(value)
                )
        return attrs

    def validate(self, attrs):
        user = authenticate(username=attrs['email'], password=attrs['password'])
        if user is None:
            raise serializers.ValidationError("Email/Password mismatch.")
        return attrs
