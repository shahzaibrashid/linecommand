__author__ = 'shahzaib'

from django.template import Library
from linecommand import Clients

register = Library()


@register.filter(name='call_reports')
def call_reports(value, arg):
    _value = value
    _arg = arg
    if _value > _arg:
        return True
    else:
        return False


@register.filter(name='is_leaf_user')
def is_leaf_user(value):
    _user_id = value
    client = Clients.objects.get(user__id=_user_id)
    return client.is_leaf_node()


@register.filter(name="trx_convert")
def trx_convert(value):
    _value = value
    if _value == 1:
        return 'Manual'
    elif _value == 2:
        return 'Paypal'
    else:
        return 'Unknown'