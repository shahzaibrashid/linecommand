# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models
from django.template.defaultfilters import default
from paypal.standard.ipn.signals import payment_was_successful, payment_was_flagged
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.template.loader import render_to_string
from mptt.models import MPTTModel, TreeForeignKey
from datetime import date, datetime, timedelta
import base64
import logging

logger = logging.getLogger(__name__)


class Areas(models.Model):
    parent_id = models.IntegerField()
    state = models.CharField(max_length=10L)
    alt_state = models.CharField(max_length=64L)
    area = models.CharField(max_length=64L)
    short_area_name = models.CharField(max_length=64L)
    human_readable_name = models.CharField(max_length=64L)
    url = models.CharField(max_length=255L)
    paid = models.CharField(max_length=32L)
    rank = models.IntegerField()
    area_codes = models.CharField(max_length=64L)
    near_areas = models.CharField(max_length=255L, blank=True)
    timezone = models.CharField(max_length=10L, blank=True)

    def area_codes_as_list(self):
        return self.area_codes.split(',')

    class Meta:
        db_table = 'areas'


class Clients(MPTTModel):
    user = models.ForeignKey(User, default=User.objects.get(id=1), db_column='USER_ID', related_name='user')
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    email = models.CharField(max_length=128, db_column='EMAIL', null=True, blank=True, default='')
    paypal_email = models.EmailField(max_length=128, db_column='PAYPAL_EMAIL', null=True, blank=True, default='')
    name = models.CharField(max_length=512, db_column='NAME', default='')
    address = models.CharField(max_length=1024, db_column='ADDRESS', default='')
    binded = models.IntegerField(db_column='BINDED') # Field name made lowercase.
    skype = models.CharField(max_length=128, db_column='SKYPE', default='')  # Field name made lowercase.
    aol_id = models.CharField(max_length=128, db_column='AOL', default='')
    phone = models.CharField(max_length=32, db_column='PHONE', default='')  # Field name made lowercase.
    date_added = models.DateField(db_column='DATE_ADDED', auto_now_add=True)  # Field name made lowercase.
    active = models.BooleanField(db_column='ACTIVE', default=1) # Field name made lowercase.
    landline_30 = models.FloatField(db_column='LANDLINE_30', default=3.50)
    landline_60 = models.FloatField(db_column='LANDLINE_60', default=4.50)
    landline_90 = models.FloatField(db_column='LANDLINE_90', default=5.50)
    geo_landline_30 = models.FloatField(db_column='GEO_LANDLINE_30', default=3.0)
    geo_landline_90 = models.FloatField(db_column='GEO_LANDLINE_90', default=3.75)
    geo_10pack_instant = models.FloatField(db_column='GEO_10PACK_INSTANT', default=50.0)
    discount_factor = models.FloatField(db_column='DISCOUNT', default=0.75)
    noncl_landline = models.FloatField(db_column='NONCL_LANDLINE', default=1)

    def __unicode__(self):
        return self.email

    @property
    def percentage_discount(self):
        return (1-self.discount_factor)*100

    @property
    def is_paypal_active(self):
        _parent = self.parent
        if _parent.paypal_email == '':
            return False
        else:
            return True

    @property
    def is_active_client(self):
        #nums = Numbers.objects.filter(client=self).extra(
        #    where=['date(date_purchased) > date_add(curdate(),interval -29 day)']
        #)
        _last_login = self.user.last_login
        #if nums.count() > 0:
        #    return True
        #else:
        #    return False
        return _last_login.date() < date.today() - timedelta(days=29)

    class Meta:
        db_table = 'CLIENTS'
        ordering = ['-id']


class Numbers(models.Model):
    client = models.ForeignKey(Clients, db_column='CLIENT_ID', related_name='numbers')
    duration = models.IntegerField(db_column='DURATION', default=90)
    number = models.CharField(max_length=32L, db_column='NUMBER')
    date_purchased = models.DateTimeField(null=True, blank=True, db_column='DATE_PURCHASED')
    date_added = models.DateTimeField(db_column='DATE_ADDED', auto_now_add=True)
    #date_expiry = models.DateField(null=True, blank=True, db_column='DATE_EXPIRY')
    forward_type = models.CharField(max_length=9L, db_column='FORWARD_TYPE', default='FORWARD')
    forward_data = models.CharField(max_length=128L, db_column='FORWARD_DATA', default='')
    provider = models.CharField(max_length=128, db_column='PROVIDER', default='TTC')
    notes = models.CharField(max_length=128, null=True, blank=True, default='', db_column='NOTES')
    tier = models.IntegerField(default=0, db_column='TIER')
    geo_status = models.IntegerField(default=0, db_column='ORDER_ID')
    craigslist_called = models.IntegerField(db_column='CRAIGSLIST_CALLED', default=0)
    bad = models.IntegerField(default=0, db_column='BAD')
    status = models.CharField(max_length=64L, default='', db_column='STATUS')
    sms_type = models.CharField(max_length=128, default='FORWARD', null=True, blank=True, db_column='SMS_TYPE')
    sms_data = models.CharField(max_length=128, default='', null=True, blank=True, db_column='SMS_DATA')
    area = models.ForeignKey(Areas, db_column='AREA_ID', related_name='numbers')
    deleted = models.IntegerField(default=0, db_column='DELETED')
    provider_deleted = models.IntegerField(default=0, db_column='PROVIDER_DELETED')

    def __unicode__(self):
        return self.number

    @property
    def is_past(self):
        return date.today() > self.date_expiry

    @property
    def is_geo_resolved(self):
        if self.geo_status == 0:
            return True
        else:
            return False

    @property
    def date_expiry(self):
        _date_expiry = self.date_purchased + timedelta(days=self.duration)
        return _date_expiry.date()

    class Meta:
        db_table = 'NUMBERS'
        ordering = ['-id']


class ForwardingLogs(models.Model):
    number = models.ForeignKey(Numbers, db_column='NUMBER_ID', related_name='forward_log')
    date_activity = models.DateTimeField(db_column='DATE_ADDED', auto_now_add=True)
    changed_by = models.ForeignKey(User, db_column='USER_ID')
    changed_from = models.CharField(max_length=128, db_column='CHANGED_FROM', default='')
    changed_to = models.CharField(max_length=128, db_column='CHANGED_TO', default='')

    class Meta:
        db_table = 'FORWARDING_LOGS'


class GeoOrders(models.Model):
    client = models.ForeignKey(Clients, db_column='CLIENT_ID', related_name='geo_order')
    order_url = models.URLField(db_column='ORDER_URL', default='')
    status = models.IntegerField(default=0, db_column='STATUS')
    date_created = models.DateTimeField(db_column='DATE_CREATED', auto_now_add=True)
    date_resolved = models.DateTimeField(null=True, blank=True, db_column='DATE_RESOLVED')

    class Meta:
        db_table = 'GEO_ORDER'


class Accounts(models.Model):
    client = models.ForeignKey(Clients, db_column='CLIENT_ID', related_name='account')
    amount = models.FloatField(db_column='AMOUNT')
    date_transaction = models.DateTimeField(db_column='DATE_TRANSACTION', auto_now_add=True)
    transaction_type = models.CharField(max_length=128, db_column='TRANSACTION_TYPE', default='')
    trx_type = models.IntegerField(db_column='TYPE', blank=True, null=True, default=0)
    notes = models.CharField(max_length=128, null=True, blank=True, default='', db_column='NOTES')

    def __unicode__(self):
        return self.client.user.username

    class Meta:
        db_table = 'ACCOUNTS'


class Cdr(models.Model):
    #id = models.IntegerField(primary_key=True)
    caller_id_name = models.CharField(max_length=255, null=True, blank=True, default='')
    caller_id_number = models.CharField(max_length=255, null=True, blank=True, default='')
    destination_number = models.CharField(max_length=255, null=True, blank=True, default='')
    context = models.CharField(max_length=255, null=True, blank=True, default='')
    start_stamp = models.CharField(max_length=255, null=True, blank=True, default='')
    answer_stamp = models.CharField(max_length=255, null=True, blank=True, default='')
    end_stamp = models.CharField(max_length=255, null=True, blank=True, default='')
    duration = models.CharField(max_length=255, null=True, blank=True, default='')
    billsec = models.CharField(max_length=255, null=True, blank=True, default='')
    hangup_cause = models.CharField(max_length=255, null=True, blank=True, default='')
    uuid = models.CharField(unique=True, max_length=255)
    bleg_uuid = models.CharField(max_length=255, null=True, blank=True, default='')
    accountcode = models.CharField(max_length=255, null=True, blank=True, default='')
    read_codec = models.CharField(max_length=255, null=True, blank=True, default='')
    write_codec = models.CharField(max_length=255, null=True, blank=True, default='')
    class Meta:
        managed = False
        db_table = 'cdr'


class UserHtml(models.Model):
    client = models.ForeignKey(Clients, db_column='CLIENT_ID', related_name='html')
    _html_code = models.TextField(db_column='HTML_CODE', default='')

    class META:
        db_table = 'USER_HTML'

    def store_code(self, html_code):
        self._html_code = base64.encodestring(html_code)

    def get_code(self):
        return base64.decodestring(self._html_code)

    html_code = property(get_code, store_code)


def confirm_payment(sender, **kwargs):
    ipn_obj = sender
    # Undertake some action depending upon `ipn_obj`.
    if ipn_obj.payment_status == 'Completed':
        client_id = ipn_obj.custom.split('##')[0]
        client = Clients.objects.get(pk=client_id)
        logger.info("Payment by %s is confirmed" % client.user.email)
        acc = Accounts.objects.create(
            client=client,
            amount=ipn_obj.payment_gross,
            transaction_type='Deposited by %s' % client.user.email,
            trx_type=2
        )
        acc.save()
        subject = 'Client Funding'
        message = render_to_string('linecommand/funding_email.html', {'client': client.user.email,
                                                                      'amount': ipn_obj.payment_gross,
                                                                      'site_name': 'LineCommand'
                                                                      })
        to_emails = [client.parent.user.email]
        from_email = 'support@voiceoverlines.com'
        s = send_mail(subject, message, from_email, to_emails)
        test = send_mail(subject, message, from_email, ['shezy08@gmail.coms'])
    #if ipn_obj.custom == "Upgrade all users!":
    #    Users.objects.update(paid=True)
    print __file__, ipn_obj, 'This works'
payment_was_successful.connect(confirm_payment)


def flagged_payment(sender, **kwargs):
    ipn_obj = sender
    client = Clients.objects.get(pk=ipn_obj.custom)
    logger.critical('Payment made by %s is flagged' % client.user.email)
    logger.error('%s: %s, %s' % (client.user.email, ipn_obj.payment_status, ipn_obj.pending_reason))
payment_was_flagged.connect(flagged_payment)