from datetime import datetime, timedelta
from django.http import HttpResponse
from django.db.models import Sum
from rest_framework import status, generics
from linecommand import bandwidth_v2, Numbers, Accounts
from linecommand.serializers import GeoNumbersOrder, NumbersSerializer, SnippetSerializer
from linecommand.views import validate_number
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer, YAMLRenderer
from linecommand.models import Clients, GeoOrders, Areas
from django.db import connection


def dictfetchall(cursor):
    """Returns all rows from a cursor as a dict"""
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


def get_number(state_url):
    get_number_query = '''select NUMBERS.id as id,areas.url as url from NUMBERS,areas where areas.id=NUMBERS.area_id
            and client_id=1 and provider='INETWORK' and deleted=0 and DATE_ADDED < DATE_SUB(NOW(), INTERVAL 1 HOUR)
            order by INSTR(%s, areas.state) DESC, DATE_ADDED ASC LIMIT 1;'''

    area = Areas.objects.filter(url=state_url)
    cursor = connection.cursor()
    cursor.execute(get_number_query, (area[0].state,))
    number = dictfetchall(cursor)
    cursor.close()
    return number


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


def pre_cache(areas, quantity, no_cache=False, nums=[]):
    inetwork = bandwidth_v2.INetwork()
    nums_list = {}
    res_code, pending, order_url, message = inetwork.Order_Numbers(quantity, areas.area_codes, nums_list)
    if res_code:
        if not len(nums_list) > 0:
            return 'Numbers are out of stock for this area code'
        if pending:
            new_order = GeoOrders.objects.create(client=Clients.objects.get(pk=1),
                                                 order_url=order_url, status=1)
            new_order.save()
            for num in nums_list:
                n = Numbers.objects.create(number=num,
                                           client=Clients.objects.get(pk=1),
                                           tier=nums_list.get(num, 0),
                                           duration=30,
                                           forward_type='FORWARD',
                                           forward_data='',
                                           provider='INETWORK',
                                           area=areas,
                                           geo_status=new_order.id)
                n.save()
        else:
            for num in nums_list:
                n = Numbers.objects.create(number=num, client=Clients.objects.get(pk=1),
                                           tier=nums_list.get(num, 0),
                                           duration=30,
                                           forward_type='FORWARD',
                                           forward_data='',
                                           provider='INETWORK',
                                           area=areas)
                n.save()
        if no_cache:
            nums.extend(nums_list.keys())
        return True
    return message


class Orders(generics.ListCreateAPIView):
    """
    Provides access to all orders within the system.
    """
    renderer_classes = (JSONRenderer,)

    def get(self, request):
        """
        Return a list of all orders.
        """
        clients = Clients.objects.all()
        #data = {'api_key': 'yahpoooooo!!!', 'count': 10, 'forward_type': 'forward', 'forward_data': '234-456-6789'}
        #serialize = customSerializer(data)
        serialize = SnippetSerializer(clients, many=True)
        content = {'clients': serialize.data}
        return Response(content)

    def post(self, request):
        """
        Order Geo Numbers.
        """
        #print request.DATA
        order = GeoNumbersOrder(data=request.DATA)

        #print order.data
        if order.is_valid():
            order_data = order.data
            #print order_data
            client = Clients.objects.get(user__email=order_data['email'])
            limit = order_data['count']
            duration = order_data['duration']
            validate_ancestors = client.get_ancestors(True, True).annotate(total_amount=Sum('account__amount'))
            for obj in validate_ancestors:
                if obj.user.is_superuser:
                    continue
                if duration == 30:
                    price = obj.geo_landline_30
                else:
                    price = obj.geo_landline_90

                if not obj.total_amount >= (price * limit):
                    return Response({"errors": "Insufficient balance"}, status=status.HTTP_400_BAD_REQUEST)
            if order_data['forward_type'] == "forward":
                forward_type = "FORWARD"
                res_code, forward_data = validate_number(order_data['forward_data'])
            else:
                forward_type = "VOICEMAIL"
                res_code, forward_data = validate_number(order_data['forward_data'], False)
            if not res_code:
                return Response({"errors": "Invalid value for forward_data"}, status=status.HTTP_400_BAD_REQUEST)
            date_purchased = datetime.now()
            #date_expiry = date_purchased + timedelta(days=duration)
            #areas = Areas.objects.get(url=order_data['area_code'])
            #numbers = Numbers.objects.filter(area=areas, client__id=1, deleted=0, provider='INETWORK').order_by('id')[:limit]
            if order_data['no_cache']:
                areas = Areas.objects.get(url=order_data['area_code'])
                nums_list = []
                message = pre_cache(areas, limit, True, nums_list)
                if message is not True:
                    return Response(
                        {"errors": message},
                        status=status.HTTP_400_BAD_REQUEST
                    )
                numbers = Numbers.objects.filter(number__in=nums_list)
            else:
                number_data = get_number(order_data['area_code'])
                if number_data:
                    if number_data[0]['url'] == order_data['area_code']:
                        numbers = Numbers.objects.filter(id=number_data[0]['id'])
                        message = pre_cache(numbers[0].area, limit)
                        if message is not True:
                            return Response(
                                {"errors": message},
                                status=status.HTTP_400_BAD_REQUEST
                            )
                    elif (not number_data[0]['url'] == order_data['area_code']) and order_data['fallback']:
                        numbers = Numbers.objects.filter(id=number_data[0]['id'])
                        message = pre_cache(numbers[0].area, limit)
                        if message is not True:
                            return Response(
                                {"errors": message},
                                status=status.HTTP_400_BAD_REQUEST
                            )
                    else:
                        areas = Areas.objects.get(url=order_data['area_code'])
                        message = pre_cache(areas, limit+2)
                        #message = pre_cache(numbers[0].area, limit)
                        if message is not True:
                            return Response(
                                {"errors": message},
                                status=status.HTTP_400_BAD_REQUEST
                            )
                        numbers = Numbers.objects.filter(
                            area=areas, client__id=1, deleted=0, provider='INETWORK'
                        ).order_by(
                            'id'
                        )[:limit]

                #if numbers.count() >= limit:
                #    quantity = limit
                #else:
                #    quantity = limit + (limit - numbers.count())
                else:
                    areas = Areas.objects.get(url=order_data['area_code'])
                    message = pre_cache(areas, limit+2)
                    if message is not True:
                        return Response(
                            {"errors": message},
                            status=status.HTTP_400_BAD_REQUEST
                        )
                    numbers = Numbers.objects.filter(
                        area=areas, client__id=1, deleted=0, provider='INETWORK'
                    ).order_by(
                        'id'
                    )[:limit]
            for num in numbers:
                num.client = client
                num.duration = duration
                num.date_purchased = date_purchased
                num.forward_type = forward_type
                num.forward_data = forward_data
                if order_data['sms_type']:
                    num.sms_type = str(order_data['sms_type']).upper()
                if order_data['sms_data']:
                    num.sms_data = order_data['sms_data']
                num.save()
            #numbers.update(
            #    client=client,
            #    duration=duration,
            #    date_purchased=date_purchased,
            #    forward_type=forward_type,
            #    forward_data=forward_data,
            #)
            #if order_data['sms_forward_type']:
            #    numbers.update(
            #        sms_type=str(order_data['sms_forward_type']).upper()
            #    )
            #if order_data['sms_data']:
            #    numbers.update(
            #        sms_data=order_data['sms_data']
            #    )
            count_purchase = len(numbers)
            response_data = {}
            response_data['NumbersRequested'] = limit
            response_data['order'] = 'completed'
            response_data['NumbersPurchased'] = count_purchase
            for obj in validate_ancestors:
                if obj.user.is_superuser:
                    continue
                trans_type = '%d Numbers purchased by %s' % (count_purchase, client.user.email)
                if duration == '30':
                    price = obj.geo_landline_30
                else:
                    price = obj.geo_landline_90
                transaction = Accounts.objects.create(
                    client=obj,
                    amount=-(price * count_purchase),
                    transaction_type=trans_type
                )
                transaction.save()
            numbers_list = []
            for num in numbers:
                numbers_list.append(num.id)
            numbers_to_serialize = Numbers.objects.filter(id__in=numbers_list)
            numbers_serialized = NumbersSerializer(numbers_to_serialize, many=True)
            response_data['numbers'] = numbers_serialized.data
            print response_data
            return Response({"Order": response_data}, status=status.HTTP_201_CREATED)
        else:
            return Response(order.errors, status=status.HTTP_400_BAD_REQUEST)
