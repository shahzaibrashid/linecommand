from django import forms
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from linecommand.models import Clients, UserHtml
from django.db.models import Sum
import hashlib


class SignUpForm(forms.Form):
    email = forms.EmailField(required=True, label='*Email', help_text='Your e-mail address',
                             widget=forms.TextInput(attrs={'placeholder': 'Your E-mail Address'}))
    password = forms.CharField(min_length=6, label='*Password', widget=forms.PasswordInput)
    name = forms.CharField(required=True, widget=forms.TextInput(attrs={'placeholder': 'Full Name'}))
    billing_address = forms.CharField(widget=forms.Textarea, label='Billing Address', required=False)
    skype_id = forms.CharField(label='Skype ID', required=False,
                               widget=forms.TextInput(attrs={'placeholder': 'Your skype ID'}))
    aol_id = forms.CharField(label='AOL ID', required=False,
                             widget=forms.TextInput(attrs={'placeholder': 'Your AOL ID'}))
    phone = forms.CharField(required=False,
                            widget=forms.TextInput(attrs={'placeholder': 'Your Contact Number'}))


class ReferalSignUpForm(SignUpForm):
    def save(self, client=Clients.objects.get(pk=1), commit=True):
        username = hashlib.md5(self.cleaned_data['email']).hexdigest()[2:]
        new_user = User.objects.create_user(
            username=username,
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password']
        )
        new_user.save()
        new_client = Clients.objects.create(
            name=self.cleaned_data['name'],
            address=self.cleaned_data['billing_address'],
            skype=self.cleaned_data['skype_id'],
            aol_id=self.cleaned_data['aol_id'],
            phone=self.cleaned_data['phone'],
            binded=0,
            user=new_user,
            parent=client
        )
        new_client.save()
        return new_client


class CreateClient(forms.Form):
    email = forms.EmailField(required=True, label='*Email', help_text='Your e-mail address',
                             widget=forms.TextInput(attrs={'placeholder': 'Your E-mail Address'}))
    password = forms.CharField(min_length=6, label='*Password', widget=forms.PasswordInput)
    skype = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Your Skype ID'}))
    phone = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Your Contact Number'}))
    active = forms.BooleanField(label="*Active", required=True)
    price_30 = forms.FloatField(required=True, label="*Price for 30 days Random number", widget=False)
    price_60 = forms.FloatField(required=True, label="*Price for 60 days Random number", widget=False)
    price_90 = forms.FloatField(required=True, label="*Price for 90 days Random number", widget=False)
    geo30_price = forms.FloatField(required=True, label="*Price for 30 days Geo number", widget=False)
    geo90_price = forms.FloatField(required=True, label="*Price for 90 days Geo number", widget=False)
    noncl_price = forms.FloatField(required=True, label="*Price for 7 day temporary number", widget=False)
    discount = forms.FloatField(required=True, label="*Percentage discount on renewals",
                                min_value=0.0, max_value=100.0,
                                widget=forms.TextInput(attrs={'placeholder': '% value from 0.0 to 100.0'}))

    class Meta:
    #    model = Clients
        fields = ['email', 'password', 'skype', 'phone', 'price_30', 'price_60', 'price_90', 'active']
        #    widgets = {
        #               'password': forms.PasswordInput(),
        #              }


class DepositForm(forms.Form):
    # client = forms.ChoiceField(widget=forms.Select(attrs={'style':'width:220px'}), label="Client")
    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    current_amount = forms.CharField(widget=forms.TextInput(attrs={'readonly': 'readonly'}), label="Current Balance")
    amount = forms.FloatField(required=True, label="*Specify Amount", widget=False)

    def __init__(self, children_list, *args, **kwargs):
        super(DepositForm, self).__init__(*args, **kwargs)
        # self.fields['client'].choices = [(int(obj.id), str(obj.user)) for obj in children_list]
        self.fields['client'].initial = children_list[0].id
        initial_amount = children_list.annotate(total=Sum('account__amount'))[0].total
        if not initial_amount:
            display_amount = 0.0
        else:
            display_amount = initial_amount
        self.fields['current_amount'].initial = display_amount

    class Meta:
        # model = Accounts
        fields = ['client', 'current_amount', 'amount']


class ChangePrice(forms.Form):
    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    price_30 = forms.FloatField(required=True, label='*Price for 30 days random', widget=False)
    price_60 = forms.FloatField(required=True, label='*Price for 60 days random', widget=False)
    price_90 = forms.FloatField(required=True, label='*Price for 90 days random', widget=False)
    geo_30 = forms.FloatField(required=True, label='*Price for 30 days geo number', widget=False)
    geo_90 = forms.FloatField(required=True, label='*Price for 90 days geo number', widget=False)
    noncl_price = forms.FloatField(required=True, label="*Price for 7 day temporary number", widget=False)
    discount = forms.FloatField(required=True, min_value=0.0, max_value=100.0,
                                label="*Percentage discount on renewals",
                                widget=forms.TextInput(attrs={'placeholder': '% value from 0.0 to 100.0'}))

    class Meta:
        fields = ['client', 'price_30', 'price_60', 'price_90', 'geo_30', 'geo_90']


class AddNumbers(forms.Form):
    choice_forward = (('FORWARD', 'FORWARD'),
                      ('VOICEMAIL', 'VOICEMAIL'))
    choice_duration = ((90, '90 Days'),
                       (60, '60 Days'),
                       (30, '30 Days'))
    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    count = forms.IntegerField(min_value=0, required=True, label="*Count (Number of lines to order)",
                               widget=forms.TextInput(attrs={'style': 'width:30px'}))
    forward = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:120px'}), choices=choice_forward)
    forward_data = forms.CharField(max_length=100, required=True, label="*Forward Data",
                                   widget=forms.TextInput(attrs={'placeholder': '202xxxxxxx or p@abc.com'}))
    duration = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:95px'}), choices=choice_duration)
    notes = forms.CharField(max_length=128, required=False, label='Note',
                            widget=forms.TextInput(attrs={'placeholder': 'Enter a note for purchase.'}))

    def __init__(self, children_list, *args, **kwargs):
        super(AddNumbers, self).__init__(*args, **kwargs)
        self.fields['client'].initial = children_list[0].id

    class Meta:
        fields = ['count', 'forward', 'forward_data', 'duration']


class BalanceForm(forms.Form):
    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    amount = forms.FloatField(required=True, label="*Specify Amount", widget=False)

    def __init__(self, children_list, *args, **kwargs):
        super(BalanceForm, self).__init__(*args, **kwargs)
        self.fields['client'].initial = children_list[0].id

    class Meta:
        # model = Accounts
        fields = ['client', 'amount']


class UpdateNumbersForm(forms.Form):
    choice_forward = (('FORWARD', 'FORWARD'),
                      ('VOICEMAIL', 'VOICEMAIL'))
    forward = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:120px'}), choices=choice_forward)
    forward_data = forms.CharField(max_length=100, required=True, label="*Forward Data",
                                   widget=forms.TextInput(attrs={'placeholder': '202xxxxxxx or p@abc.com'}))

    class Meta:
        fields = ['forward', 'forward_data']


class UpdateSMSTypeForm(forms.Form):
    choice_sms_type = (('EMAIL', 'Email'),
                      ('WEB_REQUEST', 'Web Request'),
                       ('FORWARD', 'Forward'))
    sms_type = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:120px'}), choices=choice_sms_type)
    sms_data = forms.CharField(max_length=100, required=True, label="*SMS Data",
                               widget=forms.TextInput())

    class Meta:
        fields = ['forward', 'forward_data']


class RenewNumbersForm(forms.Form):
    choice_duration = ((90, '90 Days'),
                       (60, '60 Days'),
                       (30, '30 Days'))
    duration = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:95px'}), choices=choice_duration)

    class Meta:
        fields = ['duration']


class ChangeUserForm(forms.Form):
    old_username = forms.CharField(required=True, label="Old Email")
    new_username = forms.EmailField(required=True, label="New Email")

    class Meta:
        fields = ['old_username', 'new_username']


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(required=True, label="Old Password", widget=forms.PasswordInput)
    new_password = forms.CharField(required=True, label="New Password", widget=forms.PasswordInput)

    class Meta:
        fields = ['old_password', 'new_password']


class ExportNumbers(forms.Form):
    choice_export = (('0', "Today's"),
                     ('1', 'AllNumbers'),
                     ('2', 'Selected'))
    export = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:120px'}), choices=choice_export)

    class Meta:
        fields = ['export']


class RandomNumbersForm(forms.Form):
    choice_forward = (('FORWARD', 'FORWARD'),
                      ('VOICEMAIL', 'VOICEMAIL'))
    duration = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    count = forms.IntegerField(min_value=0, required=True, label="*Count (Number of lines to order)",
                               widget=forms.TextInput(attrs={'style': 'width:30px'}))
    forward = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:120px'}), choices=choice_forward)
    forward_data = forms.CharField(max_length=100, required=True, label="*Forward Data",
                                   widget=forms.TextInput(attrs={'placeholder': '202xxxxxxx or p@abc.com'}))
    notes = forms.CharField(max_length=128, required=False, label='Note',
                            widget=forms.TextInput(attrs={'placeholder': 'Enter a note for purchase.'}))

    def clean(self):
        super(forms.Form, self).clean()
        if 'forward' in self.cleaned_data and 'forward_data' in self.cleaned_data:
            frwd = self.cleaned_data['forward']
            frwd_data = self.cleaned_data['forward_data']
            if frwd == 'VOICEMAIL':
                try:
                    validate_email(frwd_data)
                except Exception as err:
                    msg = err.messages
                    self._errors['forward_data'] = self.error_class(msg)
                    raise ValidationError(u'Enter a valid email address.')
            return self.cleaned_data

    class Meta:
        fields = ['duration', 'client', 'count', 'forward', 'forward_data']


class ImportNumbers(forms.Form):
    file = forms.FileField()

    class Meta:
        fields = ['file']


class DynamicChoiceField(forms.ChoiceField):
    def clean(self, value):
        return value


class AddGeo(forms.Form):
    choice_forward = (('FORWARD', 'FORWARD'),
                      ('VOICEMAIL', 'VOICEMAIL'))
    choice_duration = ((90, '90 Days'),
                       (30, '30 Days'))

    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    count = forms.IntegerField(min_value=0, required=True, label="*Count (Number of lines to order)",
                               widget=forms.TextInput(attrs={'style': 'width:30px'}))
    states = forms.ChoiceField(widget=forms.Select(attrs={'id': 'states', 'onchange': 'FilterModels();'}))
    city = DynamicChoiceField(widget=forms.Select(attrs={'id': 'areacodes', 'disabled': 'true'}),
                              choices=(('-1', 'Select AreaCodes'),))
    forward = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:120px'}), choices=choice_forward)
    forward_data = forms.CharField(max_length=100, required=True, label="*Forward Data",
                                   widget=forms.TextInput(attrs={'placeholder': '202xxxxxxx or p@abc.com'}))
    duration = forms.ChoiceField(widget=forms.Select(attrs={'style': 'width:95px'}), choices=choice_duration)
    notes = forms.CharField(max_length=128, required=False, label='Note',
                            widget=forms.TextInput(attrs={'placeholder': 'Enter a note for purchase.'}))

    def __init__(self, children_list, states_list, *args, **kwargs):
        super(AddGeo, self).__init__(*args, **kwargs)
        choice_states = []
        for obj in states_list:
            #state = obj.human_readable_name
            state_code = obj['state']
            choice_states.append((state_code, state_code))
        self.fields['states'].choices = choice_states
        self.fields['client'].initial = children_list[0].id

    def clean(self):
        super(forms.Form, self).clean()
        if 'forward' in self.cleaned_data and 'forward_data' in self.cleaned_data:
            frwd = self.cleaned_data['forward']
            frwd_data = self.cleaned_data['forward_data']
            msg = u"Invalid Email"
            if frwd == 'VOICEMAIL':
                try:
                    validate_email(frwd_data)
                except Exception as err:
                    msg = err.messages
                    self._errors['forward_data'] = self.error_class(msg)
                    raise ValidationError(u'Enter a valid email address.')
            return self.cleaned_data

    class Meta:
        fields = ['count', 'states', 'city', 'forward', 'forward_data', 'duration']


class fund_account(forms.Form):
    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    amount = forms.FloatField(min_value=0, required=True, label="*Specify Amount", widget=False)

    class Meta:
        fields = ['client', 'amount']


class paypal_api(forms.Form):
    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    paypal_email = forms.EmailField(required=False, label="Paypal Email")

    #def __init__(self, children_list, *args, **kwargs):
    #    super(BalanceForm, self).__init__(*args, **kwargs)
    #    self.fields['client'].initial = children_list[0].id

    class Meta:
        fields = ['client', 'paypal_email']


class HtmlCode(forms.Form):
    client = forms.IntegerField(widget=forms.HiddenInput(attrs={'readonly': 'readonly'}))
    html = forms.CharField(required=False, widget=forms.Textarea())

    #def save(self):
    #    _client = Clients.objects.get(pk=self.client)
    #    user_html = UserHtml.objects.create(client=_client)
    #    user_html.store_code(self.html)
    #    user_html.save()
    #    return user_html